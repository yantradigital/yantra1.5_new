<?php  
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

	function esa_vzaarmedia_management( $msg = '' )  {	
	$current_user = wp_get_current_user();
	//print_r ($current_user) ;
		$userid = get_current_user_id();
		//print_r ($userid);
		
		global $wpdb, $vzaar;
		$prefix=$wpdb->base_prefix;
		$error=array();
		
		
		
		if( current_user_can('editor') || current_user_can('administrator') ) { 	
			$querystr = "SELECT * FROM ".$prefix."terms";
			$channels = $wpdb->get_results($querystr, OBJECT);
			
			$sql = "SELECT * FROM ".$prefix."vzaar_videos where status = '1' ORDER BY id desc";
			$vlist = $wpdb->get_results($sql, OBJECT);
			
			$sql = "SELECT * FROM ".$prefix."memberships_level";
			$mem_level = $wpdb->get_results($sql, OBJECT);
		}
		else
		{
			$querystr = "SELECT * FROM ".$prefix."terms";
			$channels = $wpdb->get_results($querystr, OBJECT);
			
			$sql = "SELECT * FROM ".$prefix."vzaar_videos where user_id = ". $userid . " and status = '1' ORDER BY id desc";
			$vlist = $wpdb->get_results($sql, OBJECT);
			
			$sql = "SELECT * FROM ".$prefix."memberships_level";
			$mem_level = $wpdb->get_results($sql, OBJECT);
		}
		
		$vmsg = '';
		
		//Vzaar API Info Block
		require_once(dirname (__FILE__) . '/vzaar/Vzaar.php');
		
		$options = get_option('vzaar_options');
		Vzaar::$token = $options['vzaar_apikey']; 
		Vzaar::$secret = $options['vzaar_apisecret'];
		
		//Set Return URL
		$redirect_url = site_url( "wp-admin/admin.php?page=vzaarMediaAdd", 'admin');
		
		//Find API Signature
		$uploadSignature = Vzaar::getUploadSignature($redirect_url);
		$signature = $uploadSignature['vzaar-api'];
		
		//Set Media Title/Description
		/*$title = (isset($_POST['title'])) ? addslashes($_POST['title']) : '';
		$description = (isset($_POST['description'])) ? addslashes($_POST['description']) : '';*/
		
		//Video Add Block
		/*if (isset($_GET['guid'])) {
			$apireply = Vzaar::processVideo($_GET['guid'], $title, $description, 1);   //print_r('Video ID: '.$apireply); 
			$_SESSION['apireply'] = $apireply ;
			$vmsg .= 'Uploaded Media ID: ' . $apireply . ', Vzaar is processing your media file so it will take few muments to show in your media list.';
			$addfields = Vzaar::editVideo($apireply, $_GET['title'], $_GET['description']);
			Vzaar::editVideotime($apireply, $_GET['thumb_time']);
			$chanels = $_GET['vzzarmedia_category']; 
			$mlevel = $_GET['mem_level'];
			if($apireply != 0)
			{
				$sql="INSERT INTO `".$prefix."vzaar_videos` (`vzaar_id`, `channel_id`, `title`, `description`, `mem_level`, `user_id`, `date`) VALUES ('$apireply', '$chanels', '".$_GET['title']."', '".$_GET['description']."', '$userid', '$mlevel', now())";
				$result = $wpdb->query( $sql );
			}
		}*/
		
		/*$getresponse = Vzaar::getVideoDetails($apireply, true);
		print_r($getresponse) ;*/
		
		//Video Update Block
		/*$_SESSION['thumb_time']=$_POST['thumb_time'] ;*/
		
		//Video Update Block
		if( (isset($_POST['update_media'])) && ($_POST['update_media']== 'Update') ){
			$res = Vzaar::editVideo($_POST['id'], $title, $description);   
			$resa1 = Vzaar::editVideotime($_POST['id'], $_POST['thumb_time']);
			//print_r($_POST);
			$chanels = $_POST['vzzarmedia_category']; 
			$chnl = implode(",", $chanels);
			
			$mlevel = $_POST['mem_level']; 
			$ml = implode(",", $mlevel);
			
			$sql="UPDATE `".$prefix."vzaar_videos` SET `title`= '$title',`description`= '$description', channel_id='$chnl', mem_level= '$ml' WHERE vzaar_id=".$_POST['id'];
			$result = $wpdb->query( $sql );
			
			$vmsg .= 'Media ID: ' . $_POST['id'] . ' updated successfully.';
			$file='upload_file';
			if(isset($_FILES[$file]['name']))
			{
				if($_FILES[$file]['name']!='')
				{
					if ((strtolower($_FILES[$file]["type"]) == "image/gif")
					|| (strtolower($_FILES[$file]["type"]) == "image/jpeg")
					|| (strtolower($_FILES[$file]["type"]) == "image/jpg")
					|| (strtolower($_FILES[$file]["type"]) == "image/png")
					|| (strtolower($_FILES[$file]["type"]) == "image/pjpeg"))
					  {
						if ($_FILES[$file]["error"] > 0)
						{
							 echo "Error: " . $_FILES[$file]["error"] . "<br />";
						}
						else
						{
							if (!is_dir('../wp-content/uploads/vzaarthumb')) 
							{
								mkdir('../wp-content/uploads/vzaarthumb');
							}
							$status = Vzaar::uploadThumbnail($_POST['id'], $_FILES[$file]["tmp_name"]);
  							//print "Upload status: " . $status;
						}
					}
				}
			}
		}
		
		//Video Edit/Delete/Bulk Delete Block
		if( (isset($_GET['action'])) && ($_GET['action']== 'edit_video') ){
			esa_edit_vzaarvideo_info($_GET['vid'], $redirect_url);
			return;
			
		}else if( (isset($_GET['action'])) && ($_GET['action']== 'delete_video') ){
			$res = Vzaar::deleteVideo($_GET['vid']); //print_r($res);
			$sql = "DELETE FROM `".$prefix."vzaar_videos` WHERE vid = " . $_GET['vid'] ;
			$vmsg .= 'Media ID: ' . $_GET['vid'] . ' deleted successfully.';
			
		}else if( (isset($_POST['bulkaction'])) && ($_POST['bulkaction']== 'Delete') ){
			$video_ids = array();
			$video_ids = $_POST['bulkcheck'];
			foreach($video_ids as $id  ){
				$res = Vzaar::deleteVideo($id); //print_r($res);
			}
			$vmsg .= 'Media ID(s): ' . implode(', ', $video_ids) . ' deleted successfully.';
		}

		//Set Info to Retrive Media Info from Vzaar
		$title = (isset($_POST['s']) && $_POST['s'] != 'Title') ? $_POST['s'] : '';
		$labels = '';
		$count = 50;
		$page = 1;
		$sort = 'desc';
		//$status = 9;
		
		//Init Call to Vzaar for Data
		if($options['vzaar_apisecret'] != 'xxxx' && $options['vzaar_apisecret'] != 'yyyy'){
			$video_list = Vzaar::searchVideoList($options['vzaar_apisecret'], 'true', $title, $labels, $count, $page, $sort);
		}
		
		//$process_video_list = Vzaar::getVideoList($options['vzaar_apisecret'], false, $count, $labels, $status);
		//$video_detail = Vzaar::getVideoDetails($apireply); 
		//echo '<pre>';var_dump($video_list);echo '</pre>';
		
		
/*$video_id = Vzaar::uploadLink('http://ydprojects.yantrait.co.uk/2015/yantra/ytv14/wp-content/uploads/vzaar_videos/vzaar_videos-0.mp4', 'gdgf gdgdg', 'fgfgdg gdgdg');
print "Video id: " . $video_id;*/	
	
if(isset($_POST['submit']))
{
	/*$title = $_POST['title'] ;
	$description = $_POST['description'] ;
	$thumb_time = $_POST['thumb_time'];*/
	$chanels = $_POST['vzzarmedia_category']; 
	if($chanels != "")
	{
		$chnl = implode(",", $chanels);
	}
	
	$mlevel = $_POST['mem_level'];
	if($mlevel != "")
	{ 
		$ml = implode(",", $mlevel);
	}
	
	
	for($i=0;$i<count($_FILES['vzaar_videos']['name']);$i++){
		$_SESSION['videos'][$i]['file']=$_FILES['vzaar_videos']['name'][$i];
		$_SESSION['videos'][$i]['title']=$_POST['title'][$i];
		$title = $_SESSION['videos'][$i]['title'];
		$_SESSION['videos'][$i]['description']=$_POST['description'][$i];
		$description = $_SESSION['videos'][$i]['description'] ;
		$_SESSION['videos'][$i]['thumb_time']=$_POST['thumb_time'][$i];
		$thumb_time = $_SESSION['videos'][$i]['thumb_time'];
		$_SESSION['videos'][$i]['vzzarmedia_category']=$_POST['vzzarmedia_category'][$i];
		$vzzarmedia_category = $_SESSION['videos'][$i]['vzzarmedia_category'];
		$_SESSION['videos'][$i]['mem_level']=$_POST['mem_level'][$i];
		$mem_level = $_SESSION['videos'][$i]['mem_level'];
		/*$_SESSION['videos'][$i]['thumb_file']=$_FILES['upload_file']['name'][$i];*/
	}
	/*echo'<pre>';
	print_r($_SESSION);
	echo'</pre>';*/
	//exit();
	
	if(isset($_FILES))
	{
		$file='vzaar_videos';
		for($i=0;$i<count($_FILES['vzaar_videos']['name']);$i++)
		{
			$uploadDir = 'uploads/vzaar_videos';
			//$filess=array();
			if(isset($_FILES[$file]['name'][$i]))
			{
				if($_FILES[$file]['name'][$i]!='')
				{
					//print_r($file["type"]);
					if ((strtolower($_FILES[$file]["type"][$i]) == "audio/mp4")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/mp4")
					|| (strtolower($_FILES[$file]["type"][$i]) == "application/mp4")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/mpeg3")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-mpeg-3")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/mpeg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-mpeg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-f4v")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-flv")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-ms-wmv")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-ms-asf")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/mpeg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-mpeg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-mpeq2a")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-m4v")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-msvideo")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/webm")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/ogg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "application/mxf")
					|| (strtolower($_FILES[$file]["type"][$i]) == "model/vnd.mts")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-pn-realaudio")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/mj2")
					|| (strtolower($_FILES[$file]["type"][$i]) == "application/vnd.rn-realmedia")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/3gpp2")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-wav")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/wav")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/mp3")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-mp3")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-mpeg3")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/mpg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-mpg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-mpegaudio")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/m4a")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/quicktime")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/3gpp"))
					{
						if ($_FILES[$file]["error"][$i] > 0)
						{
							 echo "Error: " . $_FILES[$file]["error"][$i] . "<br />";
						}
						else
						{
							if (!is_dir('../wp-content/uploads/vzaar_videos')) 
							{
								mkdir('../wp-content/uploads/vzaar_videos');
							}
							$aliasafterimagetitle='vzaar_videos-'.$i;
							$exts=explode('.',$_FILES[$file]["name"][$i]);
							$exten='.'.$exts[count($exts)-1];
							$altername=$aliasafterimagetitle.$exten;
							move_uploaded_file($_FILES[$file]["tmp_name"][$i], "../wp-content/uploads/vzaar_videos/" . $_FILES[$file]["name"][$i]);
							rename("../wp-content/uploads/vzaar_videos/".$_FILES[$file]["name"][$i], "../wp-content/uploads/vzaar_videos/$altername");
							$vzaar_path = get_option('home')."/wp-content/uploads/vzaar_videos/". $altername ;
							//echo $vzaar_path ;
							//$vzaar_path = "/uploads/vzaar_videos/". $_FILES[$file]["name"][$i] ;
							$video_id = Vzaar::uploadLink($vzaar_path, $title, $description);
							//print "Video id: " . $video_id;
							
							
							$userid = get_current_user_id();
							$title = $_SESSION['videos'][$i]['title'];
							$description = $_SESSION['videos'][$i]['description'] ;
							$thumb_time = $_SESSION['videos'][$i]['thumb_time'];
							
							Vzaar::editVideotime($video_id, $thumb_time);
							/*$thumbfile='upload_file';
							
								if(isset($_FILES[$thumbfile]['name'][$i]))
								{
									if($_FILES[$thumbfile]['name'][$i]!='')
									{
										if ((strtolower($_FILES[$thumbfile]["type"][$i]) == "image/gif")
										|| (strtolower($_FILES[$thumbfile]["type"][$i]) == "image/jpeg")
										|| (strtolower($_FILES[$thumbfile]["type"][$i]) == "image/jpg")
										|| (strtolower($_FILES[$thumbfile]["type"][$i]) == "image/png")
										|| (strtolower($_FILES[$thumbfile]["type"][$i]) == "image/pjpeg"))
										  {
											if ($_FILES[$thumbfile]["error"][$i] > 0)
											{
												 echo "Error: " . $_FILES[$thumbfile]["error"][$i] . "<br />";
											}
											else
											{
												if (!is_dir('../wp-content/uploads/vzaarthumb')) 
												{
													mkdir('../wp-content/uploads/vzaarthumb');
												}
												//echo ($_FILES[$thumbfile]["tmp_name"][$i]) ;
												//echo "<br/>" ;
												//echo ($video_id) ;
												$status = Vzaar::uploadThumbnail($video_id, $_FILES[$thumbfile]["tmp_name"][$i]);
												//print "Upload status: " . $status;
											}
										}
									}
								}
							*/
							echo "<div style='background:#fff;margin-bottom: 20px;border-left: 4px solid #34A853;padding: 10px;margin-top: 10px;'>Uploaded Media ID: " . $video_id . ", Vzaar is processing your media file so it will take few muments to show in your media list.</div>" ;
							
							//print_r ($userid);
							$sql="INSERT INTO `".$prefix."vzaar_videos` (`vzaar_id`, `channel_id`, `title`, `description`, `mem_level`, `user_id`, `date`) VALUES ('$video_id', '$vzzarmedia_category', '$title', '$description', '$mem_level', '$userid', now())";
							$result = $wpdb->query( $sql );
						}
					}
				}
			}
		}
	}
}

	?>
	<div class="wrap">
	<?php if(!empty($vmsg)){echo '<div class="updated fade" id="message"><p>'.$vmsg.'</p></div>';}?>
	<h2><?php _e('Upload Vzaar Videos', 'vzaarvideos') ;?></h2>
<div class="mediauploader-inline">
    <form action="" name="multi_videos" enctype="multipart/form-data" method="post">
    <div class="vzaar_main input_fields_wrap">
    <div class="left_col">
    	<div class="video_field">
            <label>Title:</label>
            <input type="text" name="title[]" id="title" placeholder="Video Title"/>
        </div>
        <div class="video_field">
            <label>Description:</label>
            <textarea name="description[]" id="description" placeholder="Video Description"></textarea>
        </div>
        
        <div class="video_field">
            <label>Generate new poster frame and thumbnail:</label>
            <input type="text" name="thumb_time[]" id="thumb_time" placeholder="Time in seconds" value="1"/>
        </div>
        
       <div class="video_field">
            <label>Upload Video</label>
            <input type="file" name="vzaar_videos[]" class="vzaar_videos"/>
            <span>(file supported by vzaar asf, avi, flv, m4v, mov, mp4, m4a, 3gp, 3g2, mj2, wmv, mp3)</span>
            
        </div>
    </div>
    
    <div class="right_section">
    <div class="right_col">
        <div class="cat_heading">Channels</div>
        <div class="categories_list">
            <?php
            $categories = get_terms( 'channels', 'orderby=count&hide_empty=0' );
            //print_r ($categories);
            foreach($categories as $chnls)
            {
                echo '<div class="chnl"><input type="checkbox" name="vzzarmedia_category[]" class="vzzarmedia_category" value="'.$chnls->term_id .'"/>'.$chnls->name . '</div>';
            }
            ?>
        </div>
        </div>
    <?php
    if( current_user_can('editor') || current_user_can('administrator') ) { 
	?>
    <div class="right_col1">
        <div class="mem_heading">Membership Level</div>
        <div class="membership_list">
            <?php
            $sql = "SELECT * FROM ".$prefix."memberships_level";
			$mem_level = $wpdb->get_results($sql, OBJECT);
			foreach($mem_level as $memlevel)
            {
                echo '<div class="chnl"><input type="checkbox" name="mem_level[]" class="mem_level" value="'.$memlevel->id .'"/>'.$memlevel->name . '</div>';
            }
            ?>
        </div>
        </div>
    <?php } ?>
    </div>
   
    </div>
     <button class="add_field_button">Add More Videos</button>
    <div class="vzaar_media_button"><input type="submit" name="submit" value="Upload Video"/></div>
    </form>
</div>
    
    

<script type="text/javascript">
jQuery(document).ready(function()
{
	var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = jQuery(".input_fields_wrap"); //Fields wrapper
    var add_button      = jQuery(".add_field_button"); //Add button ID
	
	<?php
	$categories = get_terms( 'channels', 'orderby=count&hide_empty=0' );
	//$chnl = 0 ;
	foreach($categories as $chnls)
	{
		$chnl .=  '<div class="chnl"><input type="checkbox" name="vzzarmedia_category[]" class="vzzarmedia_category" value="'.$chnls->term_id .'"/>'. $chnls->name . '</div>';
		//$chnl++ ;
	}
	?>
	
	var cat = '<?php echo $chnl ?>';
	
	<?php
		//$mlevel = 0 ;
		$sql = "SELECT * FROM ".$prefix."memberships_level";
		$mem_level = $wpdb->get_results($sql, OBJECT);
		foreach($mem_level as $memlevel)
		{
			$mlevel .= '<div class="chnl"><input type="checkbox" name="mem_level[]" class="mem_level" value="'.$memlevel->id .'"/>'.$memlevel->name . '</div>';
			//$mlevel++ ;
		}
	?>
	
	var ml = '<?php echo $mlevel ?>';
	
    var x = 1; //initlal text box count
    jQuery(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
           
			jQuery(wrapper).append('<div><div class="v_left"><div class="left_col"><div class="video_field"><label>Title:</label><input type="text" name="title[]" id="title" placeholder="Video Title"/></div><div class="video_field"><label>Description:</label> <textarea name="description[]" id="description" placeholder="Video Description"></textarea></div><div class="video_field"><label>Generate new poster frame and thumbnail:</label> <input type="text" name="thumb_time[]" id="thumb_time" placeholder="Time in seconds" value="1"/></div><div class="input_fields_wrap video_field"><label>Upload Video</label><input type="file" name="vzaar_videos[]" class="vzaar_videos"/></div></div><div class="right_section"><div class="right_col"><div class="cat_heading">Channels</div><div class="categories_list">'+cat+'</div></div><div class="right_col1"><div class="mem_heading">Membership Level</div> <div class="membership_list">'+ml+'</div></div></div></div><div class="remove_field"><img src="<?php echo plugins_url();?>/vzaar-media-management/admin/images/trash.png"/></div></div>');
        }
    });
    
    jQuery(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); jQuery(this).parent('div').remove(); x--;
    })
});
</script>
    


	<hr style="color:#f7f7f7;" />

        
      

    <div class="vzaar-media-frame-content">
    
	<h2><?php _e('Vzaar Video List', 'vzaarvideos') ;?></h2>
	 <div class="vzaar-media-toolbar vzaar-filter">
        <div class="view-switch media-grid-view-switch">  
        <form id="record_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?page=vzaarMediaAdd">
        
                
                <div class="media-toolbar-main">
                	<div class="all_vzaar_videos" id="0">Show All</div>
                    <div class="vzaarall_loader"></div>
                </div>
                <div class="media-toolbar-secondary">
                	<label><strong>Filter by category:</strong> </label>
                    <select id="vzaarmedia-attachment-filters" class="vzaarmediacategory-filter attachment-category-filter filterboth" name="vzzarmedia_filter">
                    <option value="">-- Select Channel --</option>
                    <?php
					$categories = get_terms( 'channels', 'orderby=count&hide_empty=0' );
					foreach($categories as $chnls)
					{
						echo '<option value="'.$chnls->term_id.'"/>' . $chnls->name .'</option>';
					}
					?>
                    </select>
                    <div class="vzaarmedia_loader"></div>
                </div>
                <?php
                if( current_user_can('editor') || current_user_can('administrator') ) { 
				?>
                <div class="media-toolbar-secondary1">
                	<label><strong>Filter by user:</strong> </label>
                    <?php
                    $blogusers = get_users( array( 'orderby' => 'registered', 'order' => 'ASC' ));
					?>
                    <select id="vzaarmedia-user-filters" class="vzaarmediauser-filter filterboth user-filter" name="vzzarmedia_user_filter">
                    <option value="">-- Select User --</option>
                    <?php
					foreach($blogusers as $user)
					{
						echo '<option value="'.$user->ID.'"/>' . ucfirst($user->display_name) .'</option>';
					}
					?>
                    </select>
                    <div class="vzaaruser_loader"></div>
                </div>
                <?php
				}else{?><input type="hidden" name="vzaarmedia-user-filters" id="vzaarmedia-user-filters" value="" /><?php }
				?>
                <div class="media-toolbar-primary search-form">
                    <input type="text" name="s" id="media-search-input" value="Title" onfocus="if(value=='Title') value = ''" onblur="if(value=='') value = 'Title'" />
                    <input type="submit" class="button" value="Search"  />
                </div>
<script>
jQuery(document).ready(function(){
	jQuery(".all_vzaar_videos").click(function(){
		var tid = jQuery(this).attr("id");
		jQuery.post('<?php echo plugins_url() ; ?>/vzaar-media-management/admin/all_videos.php?arr=' + tid,function(data) {
			jQuery(".vzaarmedia_result").html(data);
		});
	});
			
	var x_timer;    
	jQuery(".filterboth").change(function (){
	//alert('aaa');
		var vzzarmediafilter= jQuery('.vzaarmediacategory-filter').val();
		var vzzaruser= jQuery('.vzaarmediauser-filter').val();
		//alert (vzzaruser) ;
		//alert(vzzarmediafilter);
		jQuery('.vzaarmedia_loader').show();
		jQuery(".vzaarmedia_loader").html('<img src="<?php echo plugins_url() ; ?>/vzaar-media-management/admin/images/indicator.gif" />');
		jQuery.post('<?php echo plugins_url() ; ?>/vzaar-media-management/admin/filter_categories.php', {vzzarmedia_filter:vzzarmediafilter,vzzaruser:vzzaruser}, function(data) {
			jQuery(".vzaarmedia_result").html(data);
			jQuery('.vzaarmedia_loader').hide();
		});
	}); 
});
</script>
    </div>
    </div>
    <ul class="vzaar-media-attachments vzaarmedia_result vzaaruser_result">
    	<?php 
		$i = 0;
		if(!empty($vlist)){
			foreach((array)$vlist as $video){
		?>
        	<li>
            	<div class="thumbnail">
                	<?php echo '<a href="'. $redirect_url .'&amp;action=edit_video&amp;vid='. $video->vzaar_id .'" class="edit"><img src="'."http://view.vzaar.com/".$video->vzaar_id."/image".'" title="'.$video->title.'" alt="'.$video->title.'" border="0"></a>'; ?>
            	</div>
        	</li>
        <?php
			}
		}
		else
		{
			echo '<div>No Video found</div>';
		}
		?>
    </ul>
		
	<?php
	}

	//Media Info Edit Form
	function esa_edit_vzaarvideo_info($vid, $action_url){
		$video_info = Vzaar::getVideoDetails($vid, true);
	?>
	<div class="wrap">
		<h2><?php _e('Edit Media', 'vzaarvideos') ;?></h2>
		<div class="mediauploader-inline">
        <div class="left_col">
        <form action="<?php echo $action_url;?>" name="media_upload_form" class="landing_page2_cont" enctype="multipart/form-data" method="post" onsubmit="return chkValidity(this);" id="editvmedia">
			<div class="form_action2">
            	<br /><span>Generate new poster frame and thumbnail:</span><br />
                <input type="text" name="thumb_time" id="thumb_time" placeholder="Time in seconds" value="<?php $_SESSION['thumb_time'] ;?>"/>
                <br/><strong>or</strong>
				<br /><span>Upload Thumbnail</span><br />
				<input type="file" name="upload_file" />
                <br /><span>Media ID</span><br />
				<input class="mendatory" type="text" name="id" value="<?php echo $vid; ?>" size="64" readonly="true" />
                <br /><br /><span>Media Title *</span><br />
				<input class="mendatory" type="text" name="title" id="title" value="<?php echo $video_info->title; ?>" size="64" />
				<br /><br /><span>Media Description</span><br />
				<?php /*?><input class="" type="text" name="description" id="description" value="<?php echo $video_info->description; ?>" size="64" /><?php */?>
				<textarea name="description" id="description" cols="70" rows="5"><?php echo $video_info->description; ?></textarea>
                
				<br /><br />
				<input type="submit" name="update_media" id="update_media" value="Update" />
				<br /><br />
			</div>
		
        </div>
        <div class="right_col">
    	<div class="cat_heading">Channels</div>
    	<div class="categories_list">
		<?php
       	global $wpdb, $vzaar;
		$prefix=$wpdb->base_prefix;
		$error=array();
		$categories = get_terms( 'channels', 'orderby=count&hide_empty=0' );
		
		$sql = "SELECT * FROM ".$prefix."vzaar_videos where vzaar_id = ". $vid;
		$vdetail = $wpdb->get_results($sql, OBJECT);
		$channel = $vdetail[0]->channel_id ;
		$checked_arr = array();
		if($channel !="")
		{
			$checked_arr = explode(",", $channel);
		}
		
		
		
		foreach($categories as $chnls)
        {
			$chnl_id = $chnls->term_id ;
			
			if(in_array($chnl_id, $checked_arr)){
			echo '<div class="chnl"><input type="checkbox" name="vzzarmedia_category[]" class="vzzarmedia_category" value="'.$chnls->term_id .'" checked="checked" />'.$chnls->name . '</div>';
			}else{
			
				echo '<div class="chnl"><input type="checkbox" name="vzzarmedia_category[]" class="vzzarmedia_category" value="'.$chnls->term_id .'" />'.$chnls->name . '</div>';
			}
        }
        ?>
    	</div>
    	</div>
        <div class="right_col1">
    	<div class="mem_heading">Membership Level</div>
        <div class="membership_list">
            
		<?php
       	global $wpdb, $vzaar;
		$prefix=$wpdb->base_prefix;
		$error=array();
		$sql = "SELECT * FROM ".$prefix."memberships_level";
		$mem_level = $wpdb->get_results($sql, OBJECT);
		$mid =  $mem_level[0]->id ;
		$level_checked = array();
		if($mid !="")
		{
			$level_checked = explode(",", $mid);
		}
		//print_r ($level_checked) ;
		foreach($mem_level as $memlevel)
		{
			$mid =  $mem_level[0]->id ;
			if(in_array($mid, $level_checked)){
				echo '<div class="chnl"><input type="checkbox" name="mem_level[]" class="mem_level" value="'.$memlevel->id .'" checked="checked"/>'.$memlevel->name . '</div>';
			}
			else
			{
				echo '<div class="chnl"><input type="checkbox" name="mem_level[]" class="mem_level" value="'.$memlevel->id .'"/>'.$memlevel->name . '</div>';
			}
		}
        ?>
    	</div>
    	</div>
        </form>
		</div>
	</div>
	<?php
	}
?>