<?php

/**
 * @title  Add action/filter for the upload tab 
 * @author Enamul
 */

function esa_vzaar_wp_upload_tabs ($tabs) {
	$newtab = array('vzaarmedia' => __('Vzaar Media','vzaarVIDEOS'));
    return array_merge($tabs,$newtab);
}
add_filter('media_upload_tabs', 'esa_vzaar_wp_upload_tabs');

function media_upload_vzaarmedia() {
    // Not in use
    $errors = false;
	// Generate TinyMCE HTML output
	if ( isset($_POST['send'])/* && is_array($_POST['send'])*/ ) {
		$sendA	= array_keys($_POST['send']);
		$vidKey	= (string) array_shift($sendA);
		
		// Build output:
		$html = '[vzaarmedia vid="'.$vidKey.'" height="'.$_POST['video'][$vidKey]['height'].'"  width="'.$_POST['video'][$vidKey]['width'].'" color="'.$_POST['video'][$vidKey]['color'].'"]';
		
		// Return it to TinyMCE
		return media_send_to_editor($html);
	}
		
	return wp_iframe( 'media_upload_vzaarmedia_form', $errors );
}
add_action('media_upload_vzaarmedia', 'media_upload_vzaarmedia');


function media_upload_vzaarmedia_form($errors) {
	global $wpdb, $type, $tab, $vzaar;
	$prefix=$wpdb->base_prefix;
	
	$sql = "SELECT * FROM ".$prefix."memberships_level";
	$mem_level = $wpdb->get_results($sql, OBJECT);
	 
	media_upload_header();
	$post_id 	= isset($_REQUEST['post_id'])? intval($_REQUEST['post_id']) : 0;
	$picarray 	= array();
	$vmsg 		= '';

	//Vzaar API Info Block
	require_once(dirname (__FILE__) . '/vzaar/Vzaar.php');
		
	$options = get_option('vzaar_options');
	Vzaar::$token = $options['vzaar_apikey']; 
	Vzaar::$secret = $options['vzaar_apisecret'];
	
	//Set Return URL
	$redirect_url = site_url( "wp-admin/media-upload.php?post_id=1&tab=vzaarmedia", 'admin');
	
	//Find API Signature
	$uploadSignature = Vzaar::getUploadSignature($redirect_url);
	//echo '<pre>';var_dump($uploadSignature);echo '</pre>';
	$signature = $uploadSignature['vzaar-api'];
	//echo '==========<pre>';var_dump($signature);echo '</pre>';
	//Set Media Title/Description
	/*$title = (isset($_POST['title'])) ? addslashes($_POST['title']) : '';
	$description = (isset($_POST['description'])) ? addslashes($_POST['description']) : '';*/
	
	//Video Add Block
if(isset($_POST['submit']))
{
	/*$title = $_POST['title'] ;
	$description = $_POST['description'] ;
	$thumb_time = $_POST['thumb_time'];*/
	$chanels = $_POST['vzzarmedia_category']; 
	if($chanels != "")
	{
		$chnl = implode(",", $chanels);
	}
	
	$mlevel = $_POST['mem_level'];
	if($mlevel != "")
	{ 
		$ml = implode(",", $mlevel);
	}
	
	
	for($i=0;$i<count($_FILES['vzaar_videos']['name']);$i++){
		$_SESSION['videos'][$i]['file']=$_FILES['vzaar_videos']['name'][$i];
		$_SESSION['videos'][$i]['title']=$_POST['title'][$i];
		$title = $_SESSION['videos'][$i]['title'];
		$_SESSION['videos'][$i]['description']=$_POST['description'][$i];
		$description = $_SESSION['videos'][$i]['description'] ;
		$_SESSION['videos'][$i]['thumb_time']=$_POST['thumb_time'][$i];
		$thumb_time = $_SESSION['videos'][$i]['thumb_time'];
		$_SESSION['videos'][$i]['vzzarmedia_category']=$_POST['vzzarmedia_category'][$i];
		$vzzarmedia_category = $_SESSION['videos'][$i]['vzzarmedia_category'];
		$_SESSION['videos'][$i]['mem_level']=$_POST['mem_level'][$i];
		$mem_level = $_SESSION['videos'][$i]['mem_level'];
		/*$_SESSION['videos'][$i]['thumb_file']=$_FILES['upload_file']['name'][$i];*/
	}
	/*echo'<pre>';
	print_r($_SESSION);
	echo'</pre>';*/
	//exit();
	
	if(isset($_FILES))
	{
		$file='vzaar_videos';
		for($i=0;$i<count($_FILES['vzaar_videos']['name']);$i++)
		{
			$uploadDir = 'uploads/vzaar_videos';
			//$filess=array();
			if(isset($_FILES[$file]['name'][$i]))
			{
				if($_FILES[$file]['name'][$i]!='')
				{
					//print_r($file["type"]);
					if ((strtolower($_FILES[$file]["type"][$i]) == "audio/mp4")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/mp4")
					|| (strtolower($_FILES[$file]["type"][$i]) == "application/mp4")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/mpeg3")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-mpeg-3")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/mpeg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-mpeg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-f4v")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-flv")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-ms-wmv")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-ms-asf")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/mpeg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-mpeg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-mpeq2a")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-m4v")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/x-msvideo")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/webm")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/ogg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "application/mxf")
					|| (strtolower($_FILES[$file]["type"][$i]) == "model/vnd.mts")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-pn-realaudio")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/mj2")
					|| (strtolower($_FILES[$file]["type"][$i]) == "application/vnd.rn-realmedia")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/3gpp2")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-wav")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/wav")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/mp3")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-mp3")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-mpeg3")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/mpg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-mpg")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/x-mpegaudio")
					|| (strtolower($_FILES[$file]["type"][$i]) == "audio/m4a")
					|| (strtolower($_FILES[$file]["type"][$i]) == "video/3gpp"))
					{
						if ($_FILES[$file]["error"][$i] > 0)
						{
							 echo "Error: " . $_FILES[$file]["error"][$i] . "<br />";
						}
						else
						{
							if (!is_dir('../wp-content/uploads/vzaar_videos')) 
							{
								mkdir('../wp-content/uploads/vzaar_videos');
							}
							$aliasafterimagetitle='vzaar_videos-'.$i;
							$exts=explode('.',$_FILES[$file]["name"][$i]);
							$exten='.'.$exts[count($exts)-1];
							$altername=$aliasafterimagetitle.$exten;
							move_uploaded_file($_FILES[$file]["tmp_name"][$i], "../wp-content/uploads/vzaar_videos/" . $_FILES[$file]["name"][$i]);
							rename("../wp-content/uploads/vzaar_videos/".$_FILES[$file]["name"][$i], "../wp-content/uploads/vzaar_videos/$altername");
							$vzaar_path = get_option('home')."/wp-content/uploads/vzaar_videos/". $altername ;
							//echo $vzaar_path ;
							//$vzaar_path = "/uploads/vzaar_videos/". $_FILES[$file]["name"][$i] ;
							$video_id = Vzaar::uploadLink($vzaar_path, $title, $description);
							//print "Video id: " . $video_id;
							
							
							$userid = get_current_user_id();
							$title = $_SESSION['videos'][$i]['title'];
							$description = $_SESSION['videos'][$i]['description'] ;
							$thumb_time = $_SESSION['videos'][$i]['thumb_time'];
							
							Vzaar::editVideotime($video_id, $thumb_time);
							/*$thumbfile='upload_file';
							
								if(isset($_FILES[$thumbfile]['name'][$i]))
								{
									if($_FILES[$thumbfile]['name'][$i]!='')
									{
										if ((strtolower($_FILES[$thumbfile]["type"][$i]) == "image/gif")
										|| (strtolower($_FILES[$thumbfile]["type"][$i]) == "image/jpeg")
										|| (strtolower($_FILES[$thumbfile]["type"][$i]) == "image/jpg")
										|| (strtolower($_FILES[$thumbfile]["type"][$i]) == "image/png")
										|| (strtolower($_FILES[$thumbfile]["type"][$i]) == "image/pjpeg"))
										  {
											if ($_FILES[$thumbfile]["error"][$i] > 0)
											{
												 echo "Error: " . $_FILES[$thumbfile]["error"][$i] . "<br />";
											}
											else
											{
												if (!is_dir('../wp-content/uploads/vzaarthumb')) 
												{
													mkdir('../wp-content/uploads/vzaarthumb');
												}
												//echo ($_FILES[$thumbfile]["tmp_name"][$i]) ;
												//echo "<br/>" ;
												//echo ($video_id) ;
												$status = Vzaar::uploadThumbnail($video_id, $_FILES[$thumbfile]["tmp_name"][$i]);
												//print "Upload status: " . $status;
											}
										}
									}
								}*/
							
							echo "<div style='background:#fff;margin-bottom: 20px;border-left: 4px solid #34A853;padding: 10px;margin-top: 10px;'>Uploaded Media ID: " . $video_id . ", Vzaar is processing your media file so it will take few muments to show in your media list.</div>" ;
							
							//print_r ($userid);
							$sql="INSERT INTO `".$prefix."vzaar_videos` (`vzaar_id`, `channel_id`, `title`, `description`, `mem_level`, `user_id`, `date`) VALUES ('$video_id', '$vzzarmedia_category', '$title', '$description', '$mem_level', '$userid', now())";
							$result = $wpdb->query( $sql );
						}
					}
				}
			}
		}
	}
}

	
	?>
	<div style="padding-right:17px;">
	<?php if(!empty($vmsg)){echo '<div class="updated fade" id="message"><p>'.$vmsg.'</p></div>';}?>
	<h3>Upload a video</h3>
<link rel='stylesheet' href='<?php echo  plugins_url() ;?>/vzaar-media-management/admin/css/style.css' type='text/css' media='screen' />	
    <div class="mediauploader-inline">
    <form action="" name="multi_videos" enctype="multipart/form-data" method="post">
    <div class="vzaar_main input_fields_wrap">
    <div class="left_col">
    	<div class="video_field">
            <label>Title:</label>
            <input type="text" name="title[]" id="title" placeholder="Video Title"/>
        </div>
        <div class="video_field">
            <label>Description:</label>
            <textarea name="description[]" id="description" placeholder="Video Description"></textarea>
        </div>
        
        <div class="video_field">
            <label>Generate new poster frame and thumbnail:</label>
            <input type="text" name="thumb_time[]" id="thumb_time" placeholder="Time in seconds" value="1"/>
        </div>
                        
        <div class="video_field">
            <label>Upload Video</label>
            <input type="file" name="vzaar_videos[]" class="vzaar_videos"/>
            <span>(file supported by vzaar asf, avi, flv, m4v, mov, mp4, m4a, 3gp, 3g2, mj2, wmv, mp3)</span>
        </div>
    </div>
    
    <div class="right_section">
    <div class="right_col">
        <div class="cat_heading">Channels</div>
        <div class="categories_list">
            <?php
            $categories = get_terms( 'channels', 'orderby=count&hide_empty=0' );
            //print_r ($categories);
            foreach($categories as $chnls)
            {
                echo '<div class="chnl"><input type="checkbox" name="vzzarmedia_category[]" class="vzzarmedia_category" value="'.$chnls->term_id .'"/>'.$chnls->name . '</div>';
            }
            ?>
        </div>
        </div>
    <?php
    if( current_user_can('editor') || current_user_can('administrator') ) { 
	?>
    <div class="right_col1">
        <div class="mem_heading">Membership Level</div>
        <div class="membership_list">
            <?php
            $sql = "SELECT * FROM ".$prefix."memberships_level";
			$mem_level = $wpdb->get_results($sql, OBJECT);
			foreach($mem_level as $memlevel)
            {
                echo '<div class="chnl"><input type="checkbox" name="mem_level[]" class="mem_level" value="'.$memlevel->id .'"/>'.$memlevel->name . '</div>';
            }
            ?>
        </div>
        </div>
    <?php } ?>
    </div>
    </div>
    <button class="add_field_button">Add More Videos</button>
    <div class="vzaar_media_button"><input type="submit" name="submit" value="Upload Video"/></div>
    </form>
</div>
    
    

<script type="text/javascript">
jQuery(document).ready(function()
{
	var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = jQuery(".input_fields_wrap"); //Fields wrapper
    var add_button      = jQuery(".add_field_button"); //Add button ID
	
	<?php
	$categories = get_terms( 'channels', 'orderby=count&hide_empty=0' );
	//$chnl = 0 ;
	foreach($categories as $chnls)
	{
		$chnl .=  '<div class="chnl"><input type="checkbox" name="vzzarmedia_category[]" class="vzzarmedia_category" value="'.$chnls->term_id .'"/>'. $chnls->name . '</div>';
		//$chnl++ ;
	}
	?>
	
	var cat = '<?php echo $chnl ?>';
	
	<?php
		//$mlevel = 0 ;
		$sql = "SELECT * FROM ".$prefix."memberships_level";
		$mem_level = $wpdb->get_results($sql, OBJECT);
		foreach($mem_level as $memlevel)
		{
			$mlevel .= '<div class="chnl"><input type="checkbox" name="mem_level[]" class="mem_level" value="'.$memlevel->id .'"/>'.$memlevel->name . '</div>';
			//$mlevel++ ;
		}
	?>
	
	var ml = '<?php echo $mlevel ?>';
	
    var x = 1; //initlal text box count
    jQuery(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
           
			jQuery(wrapper).append('<div><div class="v_left"><div class="left_col"><div class="video_field"><label>Title:</label><input type="text" name="title[]" id="title" placeholder="Video Title"/></div><div class="video_field"><label>Description:</label> <textarea name="description[]" id="description" placeholder="Video Description"></textarea></div><div class="video_field"><label>Generate new poster frame and thumbnail:</label> <input type="text" name="thumb_time[]" id="thumb_time" placeholder="Time in seconds" value="1"/></div><div class="input_fields_wrap video_field"><label>Upload Video</label><input type="file" name="vzaar_videos[]" class="vzaar_videos"/></div></div><div class="right_section"><div class="right_col"><div class="cat_heading">Channels</div><div class="categories_list">'+cat+'</div></div><div class="right_col1"><div class="mem_heading">Membership Level</div> <div class="membership_list">'+ml+'</div></div></div></div><div class="remove_field"><img src="<?php echo plugins_url();?>/vzaar-media-management/admin/images/trash.png"/></div></div>');
        }
    });
    
    jQuery(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); jQuery(this).parent('div').remove(); x--;
    })
});
</script>
	
    
    <hr />
<link rel='stylesheet' href='<?php echo  plugins_url() ;?>/vzaar-media-management/admin/css/style.css' type='text/css' media='screen' />
    <div class="vzaar-media-toolbar vzaar-filter">
    <div class="view-switch media-grid-view-switch"> 
	<form method="get" id="filter">
        <input type="hidden" value="video" name="type" />
        <input type="hidden" value="vzaarmedia" name="tab" />
        <input type="hidden" value="<?php echo $post_id; ?>" name="post_id" />
        <input type="hidden" value="video" name="post_mime_type" />
        <div class="media-toolbar-main">
                	<div class="all_vzaar_videos" id="0">Show All</div>
                    <div class="vzaarall_loader"></div>
                </div>
        <div class="media-toolbar-secondary">
            <label><strong>Filter by category:</strong> </label>
            <select id="vzaarmedia-attachment-filters" class="vzaarmediacategory-filter attachment-category-filter filterboth" name="vzzarmedia_filter">
            <option value="">-- Select Channel --</option>
            <?php
            $categories = get_terms( 'channels', 'orderby=count&hide_empty=0' );
            foreach($categories as $chnls)
            {
                echo '<option value="'.$chnls->term_id.'"/>' . $chnls->name .'</option>';
            }
            ?>
            </select>
            <div class="vzaarmedia_loader"></div>
        </div>
        <?php
        if( current_user_can('editor') || current_user_can('administrator') ) { 
        ?>
        <div class="media-toolbar-secondary1">
            <label><strong>Filter by user:</strong> </label>
            <?php
            $blogusers = get_users( array( 'orderby' => 'registered', 'order' => 'ASC' ));
            ?>
            <select id="vzaarmedia-user-filters" class="vzaarmediauser-filter filterboth user-filter" name="vzzarmedia_user_filter">
            <option value="">-- Select User --</option>
            <?php
            foreach($blogusers as $user)
            {
                echo '<option value="'.$user->ID.'"/>' . ucfirst($user->display_name) .'</option>';
            }
            ?>
            </select>
            <div class="vzaaruser_loader"></div>
        </div>
        <?php
        }else{?><input type="hidden" name="vzaarmedia-user-filters" id="vzaarmedia-user-filters" value="" /><?php }
        ?>
        <div class="search-box media-toolbar-primary search-form" id="media-search">
            
            <input type="text" name="s" id="media-search-input" value="Title" onfocus="if(value=='Title') value = ''" onblur="if(value=='') value = 'Title'" />
            <input type="submit" class="button" value="Search Media" />
        </div>
    </form>
    </div>
    </div>
<script>
jQuery(document).ready(function(){
	jQuery(".all_vzaar_videos").click(function(){
		var tid = jQuery(this).attr("id");
		//alert (tid);
		jQuery.post('<?php echo plugins_url() ; ?>/vzaar-media-management/admin/all_media_videos.php?arr=' + tid,function(data) {
			jQuery(".vzaarmedia_result").html(data);
			jQuery('a.describe-toggle-on').click(function(){
				jQuery(this).parent('.preloaded').addClass('open');
				jQuery(this).nextAll('table.slidetoggle').css({'display':'table'});
				return false;
			});
			jQuery('a.describe-toggle-off').click(function(){
				jQuery(this).parent('.preloaded').removeClass('open');
				jQuery(this).nextAll('table.slidetoggle').css({'display':'none'});
				return false;
			});
		});
	});
			
	var x_timer;    
	jQuery(".filterboth").change(function (){
	//alert('aaa');
		var vzzarmediafilter= jQuery('.vzaarmediacategory-filter').val();
		var vzzaruser= jQuery('.vzaarmediauser-filter').val();
		//alert (vzzaruser) ;
		//alert(vzzarmediafilter);
		jQuery('.vzaarmedia_loader').show();
		jQuery(".vzaarmedia_loader").html('<img src="<?php echo plugins_url() ; ?>/vzaar-media-management/admin/images/indicator.gif" />');
		jQuery.post('<?php echo plugins_url() ; ?>/vzaar-media-management/admin/filter_media_categories.php', {vzzarmedia_filter:vzzarmediafilter,vzzaruser:vzzaruser}, function(data) {
			jQuery(".vzaarmedia_result").html(data);
			jQuery('.vzaarmedia_loader').hide();
			jQuery('a.describe-toggle-on').click(function(){
				jQuery(this).parent('.preloaded').addClass('open');
				jQuery(this).nextAll('table.slidetoggle').css({'display':'table'});
				return false;
			});
			jQuery('a.describe-toggle-off').click(function(){
				jQuery(this).parent('.preloaded').removeClass('open');
				jQuery(this).nextAll('table.slidetoggle').css({'display':'none'});
				return false;
			});
		});
	}); 
});
</script>	    
	<p>&nbsp;</p>
	
    
    
	<?php wp_nonce_field('vzaar-media-form'); ?>

	<script type="text/javascript">
	<!--
	function chkValidity(form, location){
		var reqFields = jQuery(form).find(".mendatory");
		var err=false;
		
		//check for empty fields:
		for(i=0; i < reqFields.length; i++){ // ERR = NULL or ERR_MSG or DEFAULT_SELECT
			if( reqFields[i].value=='' || (reqFields[i].value==0 && reqFields[i].tagName.toUpperCase()=='SELECT') ){//SELECT TAG but has initial value to '0':
				err=true;
				if( jQuery(reqFields[i]).attr('type') == 'hidden' ){
					err = false; continue; //do nothing;
				}
				else if( jQuery(reqFields[i]).attr('type') == 'file' )
					alert("Please choose media file(s).");
				else if( reqFields[i].tagName.toUpperCase() == 'SELECT' ){//SELECT TAG but no initial value:
					alert('Please complete selecting [ '+jQuery(form).find("label[for='"+reqFields[i].name+"']").text()+' ]');
				}
				else{
					var alertMsg = (location=='sidebar')? 'Please complete all fields' : 'Please complete the required fields marked with (*)';
					alert(alertMsg);
				}
				break;
			}
		}
		return (err)? false : true;
	}


	jQuery(function($){
		var preloaded = $(".media-item.preloaded");
		if ( preloaded.length > 0 ) {
			preloaded.each(function(){prepareMediaItem({id:this.id.replace(/[^0-9]/g, '')},'');});
			//updateMediaForm();
		}
	});
	-->
	</script>
<div class="vzaarmedia_result">
<form enctype="multipart/form-data" method="post" action="" class="media-upload-form validate" id="library-form">
	<div id="media-items">
	<?php
	global $wpdb,$vzaar;
	$prefix=$wpdb->base_prefix;
	$title = (isset($_GET['s']) && $_GET['s'] != 'Title') ? $_GET['s'] : '';
	$labels = '';
	$count = 20;
	$page = 1;
	$sort = 'desc';
	
	//$video_list = Vzaar::getVideoList($options['vzaar_apisecret'], false, 20);
	$vids=array();
	if($options['vzaar_apisecret'] != 'xxxx' && $options['vzaar_apisecret'] != 'yyyy'){
		$video_list = Vzaar::searchVideoList($options['vzaar_apisecret'], 'true', $title, $labels, $count, $page, $sort);
		if(!empty($video_list)){foreach($video_list as $video){
			array_push($vids,$video->id);
		}}
	}
	asort($vids);
	// Get Vzaar Videos Post
	$vid=implode(',',$vids);
	//echo $sql1= "SELECT *  FROM ".$prefix."vzaar_videos WHERE `meta_key`='_vzaarvideolink' and meta_value in ($vid) ORDER BY `post_id` DESC  " ;
	$sql1= "SELECT * FROM `".$prefix."vzaar_videos` WHERE `vzaar_id` IN ($vid)";
	$result1 = $wpdb->get_results($sql1, OBJECT);
	
	//foreach($result1 as $res1){
	//echo '<pre>' ;print_r ($res1) ;echo '</pre>' ;
	$auth = true;
	if( !empty($result1) ) {
		foreach ($result1 as $i => $video){ 
			try{
				$video_detail = Vzaar::getVideoDetails($video->vzaar_id, $auth); 
			}catch (Exception $e) {
				echo 'Caught exception: ',  $e->getMessage(), "\n";
			}
			?>
			<div id="media-item-<?php echo ($i+1); ?>" class="media-item preloaded">
			  <strong class='filename'><?php echo $video->title; ?></strong>
			  <a class='toggle describe-toggle-on' href='#'><?php esc_attr( _e('Show', "vzaarVIDEOS") ); ?></a>
			  <a class='toggle describe-toggle-off' href='#'><?php esc_attr( _e('Hide', "vzaarVIDEOS") );?></a>
			  <table class="slidetoggle describe startclosed"><tbody>
              	  <tr>
					<td rowspan='2'><?php echo '<a href="https://vzaar.com/videos/'.$video->vzaar_id.'" target="_blank"><img src="http://view.vzaar.com/'.$video->vzaar_id.'/thumb" title="'.$video->title.'" alt="'.$video->title.'" border="0"></a>'; ?></td>
					<td><strong><?php esc_attr( _e('Key:', "vzaarVIDEOS") ); ?></strong> <?php echo $video->vzaar_id; ?></td>
				  </tr>
				  <td><strong><?php esc_attr( _e('Duration:', "vzaarVIDEOS") ); ?></strong> <?php echo $video_detail->duration; ?></td>
				  <?php if($video_detail->description != '' ){?>
				  <tr><td><strong><?php esc_attr( _e('Description:', "vzaarVIDEOS") ); ?></strong> <?php echo $video_detail->description; ?></td></tr>
				   <?php }else{ }?>
					<tr class="align">
						<td class="label"><label for="video[<?php echo $video->vzaar_id ?>][height]"><strong><?php esc_attr_e("Height","vzaarVIDEOS"); ?></strong></label></td>
						<td class="field" style="text-align:left">
                        	<input type="text" name="video[<?php echo $video->vzaar_id ?>][height]" id="video[<?php echo $video->vzaar_id ?>][height]" value="<?php echo $video_detail->height;?>" />
						</td>
					</tr>
					
					<tr class="align">
						<td class="label"><label for="video[<?php echo $video->vzaar_id ?>][width]"><strong><?php esc_attr_e("Width","vzaarVIDEOS"); ?></strong></label></td>
						<td class="field" style="text-align:left">
                        	<input type="text" name="video[<?php echo $video->vzaar_id ?>][width]" id="video[<?php echo $video->vzaar_id ?>][width]" value="<?php echo $video_detail->width;?>" />
						</td>
					</tr>
					
					<tr class="align">
						<td class="label"><label for="video[<?php echo $video->vzaar_id ?>][color]"><strong><?php esc_attr_e("Player Color","vzaarVIDEOS"); ?></strong></label></td>
						<td class="field" style="text-align:left">
							<select name="video[<?php echo $video->vzaar_id ?>][color]" id="video[<?php echo $video->vzaar_id ?>][color]">
								<option value="black" <?php if($vzaar->options['vzaar_player_color'] == 'black'){echo ' selected="selected"';} ?>>Black</option>
								<option value="blue" <?php if($vzaar->options['vzaar_player_color'] == 'blue'){echo ' selected="selected"';} ?>>Blue</option>
								<option value="red" <?php if($vzaar->options['vzaar_player_color'] == 'red'){echo ' selected="selected"';} ?>>Red</option>
								<option value="green" <?php if($vzaar->options['vzaar_player_color'] == 'green'){echo ' selected="selected"';} ?>>Green</option>
								<option value="yellow" <?php if($vzaar->options['vzaar_player_color'] == 'yellow'){echo ' selected="selected"';} ?>>Yellow</option>
								<option value="pink" <?php if($vzaar->options['vzaar_player_color'] == 'pink'){echo ' selected="selected"';} ?>>Pink</option>
								<option value="orange" <?php if($vzaar->options['vzaar_player_color'] == 'orange'){echo ' selected="selected"';} ?>>Orange</option>
								<option value="brown" <?php if($vzaar->options['vzaar_player_color'] == 'brown'){echo ' selected="selected"';} ?>>Brown</option>
							</select>
						</td>
					</tr>
					
                    <tr class="align">
						<td class="label" valign="top"><label for="video[<?php echo $video->vzaar_id ?>][embededcodeS]"><strong><?php esc_attr_e("Embed Code","vzaarVIDEOS"); ?></strong></label></td>
						<td class="field" style="text-align:left">
							<textarea cols="60" rows="7" readonly="readonly" name="video[<?php echo $video->vzaar_id ?>][embededcodeS]" id="video[<?php echo $video->vzaar_id ?>][embededcode]"><?php echo htmlspecialchars($video_detail->html);?></textarea>
                            <small>Copy-paste this code to your website to embed the video.</small>
						</td>
					</tr>
				   <tr class="submit">
						<td></td>
						<td class="savesend">
							<button type="submit" class="button" value="1" name="send[<?php echo $video->vzaar_id ?>]"><?php esc_html_e( 'Insert into Post' ); ?></button>
						</td>
				   </tr>
			  </tbody></table>
			</div>
		<?php		  
		}
	}
	/*}*/
	?>
	</div>
	
    <input type="hidden" name="type" value="<?php echo esc_attr( $GLOBALS['type'] ); ?>" />
	<input type="hidden" name="tab" value="<?php echo esc_attr( $GLOBALS['tab'] ); ?>" />
	<input type="hidden" name="post_id" id="post_id" value="<?php echo (int) $post_id; ?>" />
</form>
</div>
</div>
<?php
}
?>