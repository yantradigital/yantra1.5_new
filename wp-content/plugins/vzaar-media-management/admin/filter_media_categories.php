<?php
include_once('../../../../wp-config.php' );
global $wpdb,$signature;
$prefix=$wpdb->base_prefix;
$blog_id = $wpdb->blogid;
//$current_user = wp_get_current_user();
//print_r ($current_user) ;
$userid = get_current_user_id();
$cnd='';

if(isset($_POST['vzzarmedia_filter']) && trim($_POST['vzzarmedia_filter'])!=''){
	$channel_id = $_POST['vzzarmedia_filter'] ;
	$cnd.=" and channel_id LIKE ('%$channel_id%')";
}
if(isset($_POST['vzzaruser']) && trim($_POST['vzzaruser'])!=''){
	$user_id = $_POST['vzzaruser'] ;
	$cnd.=" and user_id ='$user_id'";
}

require_once(dirname (__FILE__) . '/vzaar/Vzaar.php');
	
$options = get_option('vzaar_options');
Vzaar::$token = $options['vzaar_apikey']; 
Vzaar::$secret = $options['vzaar_apisecret'];

?>

		<form enctype="multipart/form-data" method="post" action="" class="media-upload-form validate" id="library-form">
        	<div id="media-items">
            	<?php
				$title = (isset($_GET['s']) && $_GET['s'] != 'Title') ? $_GET['s'] : '';
				$labels = '';
				$count = 20;
				$page = 1;
				$sort = 'desc';
				
				//$video_list = Vzaar::getVideoList($options['vzaar_apisecret'], false, 20);
				$vids=array();
				if($options['vzaar_apisecret'] != 'xxxx' && $options['vzaar_apisecret'] != 'yyyy'){
					$video_list = Vzaar::searchVideoList($options['vzaar_apisecret'], 'true', $title, $labels, $count, $page, $sort);
					if(!empty($video_list)){foreach($video_list as $video){
						array_push($vids,$video->id);
					}}
				}
				asort($vids);
				// Get Vzaar Videos Post
				$vid=implode(',',$vids);
				
				$sql1= "SELECT * FROM `".$prefix."vzaar_videos` WHERE status = '1' $cnd and `vzaar_id` IN ($vid)";
				$result1 = $wpdb->get_results($sql1, OBJECT);
				
				$auth = true;
				if( !empty($result1) ) {
					foreach ($result1 as $i => $video){ 
						try{
							$video_detail = Vzaar::getVideoDetails($video->vzaar_id, $auth); 
						}catch (Exception $e) {
							echo 'Caught exception: ',  $e->getMessage(), "\n";
						}
						?>
						<div id="media-item-<?php echo ($i+1); ?>" class="media-item preloaded">
						  <strong class='filename'><?php echo $video->title; ?></strong>
						  <a class='toggle describe-toggle-on' href='#' id="vzvd-<?php echo $video->vzaar_id ; ?>"><?php esc_attr( _e('Show', "vzaarVIDEOS") ); ?></a>
						  <a class='toggle describe-toggle-off' href='#' id="vzvd-<?php echo $video->vzaar_id ; ?>"><?php esc_attr( _e('Hide', "vzaarVIDEOS") );?></a>
						  <table class="slidetoggle describe startclosed"><tbody>
							  <tr>
								<td rowspan='2'><?php echo '<a href="https://vzaar.com/videos/'.$video->vzaar_id.'" target="_blank"><img src="http://view.vzaar.com/'.$video->vzaar_id.'/thumb" title="'.$video->title.'" alt="'.$video->title.'" border="0"></a>'; ?></td>
								<td><strong><?php esc_attr( _e('Key:', "vzaarVIDEOS") ); ?></strong> <?php echo $video->vzaar_id; ?></td>
							  </tr>
							  <td><strong><?php esc_attr( _e('Duration:', "vzaarVIDEOS") ); ?></strong> <?php echo $video_detail->duration; ?></td>
							  <?php if($video_detail->description != '' ){?>
							  <tr><td><strong><?php esc_attr( _e('Description:', "vzaarVIDEOS") ); ?></strong> <?php echo $video_detail->description; ?></td></tr>
							   <?php }else{ }?>
								<tr class="align">
									<td class="label"><label for="video[<?php echo $video->vzaar_id ?>][height]"><strong><?php esc_attr_e("Height","vzaarVIDEOS"); ?></strong></label></td>
									<td class="field" style="text-align:left">
										<input type="text" name="video[<?php echo $video->vzaar_id ?>][height]" id="video[<?php echo $video->vzaar_id ?>][height]" value="<?php echo $video_detail->height;?>" />
									</td>
								</tr>
								
								<tr class="align">
									<td class="label"><label for="video[<?php echo $video->vzaar_id ?>][width]"><strong><?php esc_attr_e("Width","vzaarVIDEOS"); ?></strong></label></td>
									<td class="field" style="text-align:left">
										<input type="text" name="video[<?php echo $video->vzaar_id ?>][width]" id="video[<?php echo $video->vzaar_id ?>][width]" value="<?php echo $video_detail->width;?>" />
									</td>
								</tr>
								
								<tr class="align">
									<td class="label"><label for="video[<?php echo $video->vzaar_id ?>][color]"><strong><?php esc_attr_e("Player Color","vzaarVIDEOS"); ?></strong></label></td>
									<td class="field" style="text-align:left">
										<select name="video[<?php echo $video->vzaar_id ?>][color]" id="video[<?php echo $video->vzaar_id ?>][color]">
											<option value="black" <?php if($vzaar->options['vzaar_player_color'] == 'black'){echo ' selected="selected"';} ?>>Black</option>
											<option value="blue" <?php if($vzaar->options['vzaar_player_color'] == 'blue'){echo ' selected="selected"';} ?>>Blue</option>
											<option value="red" <?php if($vzaar->options['vzaar_player_color'] == 'red'){echo ' selected="selected"';} ?>>Red</option>
											<option value="green" <?php if($vzaar->options['vzaar_player_color'] == 'green'){echo ' selected="selected"';} ?>>Green</option>
											<option value="yellow" <?php if($vzaar->options['vzaar_player_color'] == 'yellow'){echo ' selected="selected"';} ?>>Yellow</option>
											<option value="pink" <?php if($vzaar->options['vzaar_player_color'] == 'pink'){echo ' selected="selected"';} ?>>Pink</option>
											<option value="orange" <?php if($vzaar->options['vzaar_player_color'] == 'orange'){echo ' selected="selected"';} ?>>Orange</option>
											<option value="brown" <?php if($vzaar->options['vzaar_player_color'] == 'brown'){echo ' selected="selected"';} ?>>Brown</option>
										</select>
									</td>
								</tr>
								
								<tr class="align">
									<td class="label" valign="top"><label for="video[<?php echo $video->vzaar_id ?>][embededcodeS]"><strong><?php esc_attr_e("Embed Code","vzaarVIDEOS"); ?></strong></label></td>
									<td class="field" style="text-align:left">
										<textarea cols="60" rows="7" readonly="readonly" name="video[<?php echo $video->vzaar_id ?>][embededcodeS]" id="video[<?php echo $video->vzaar_id ?>][embededcode]"><?php echo htmlspecialchars($video_detail->html);?></textarea>
										<small>Copy-paste this code to your website to embed the video.</small>
									</td>
								</tr>
							   <tr class="submit">
									<td></td>
									<td class="savesend">
										<button type="submit" class="button" value="1" name="send[<?php echo $video->vzaar_id ?>]"><?php esc_html_e( 'Insert into Post' ); ?></button>
									</td>
							   </tr>
						  </tbody></table>
						</div>
                                

					<?php		  
					}
				}
				else
				{
					echo "No video found" ;
				}
				?>
            </div>
		</form>
