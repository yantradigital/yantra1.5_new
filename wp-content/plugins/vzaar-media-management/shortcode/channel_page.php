<?php
include_once('../../../../wp-config.php' );
global $wpdb,$signature;
$prefix=$wpdb->base_prefix;
$blog_id = $wpdb->blogid;


if(isset($_REQUEST["arr"]))
{
	
	
	$querystr= "SELECT v.* FROM wp_vzaar_videos AS v WHERE channel_id LIKE '%".$_REQUEST["arr"]."%' ";
	$result = $wpdb->get_results($querystr, OBJECT);
	
    if($result > 0)
	{
			
        foreach($result as $vdetail){
        ?>
            <div class="video_list">
                <div class="left_column">
                    <div class="video_info">
                        <div class="video">
                        <iframe allowFullScreen allowTransparency="true" class="vzaar-video-player" frameborder="0" id="vzvd-<?php echo $vdetail->vzaar_id ; ?>" name="vzvd-<?php echo $vdetail->vzaar_id ;?>" src="http://view.vzaar.com/<?php echo $vdetail->vzaar_id ; ?>/player?autoplay=false" title="vzaar video player" type="text/html"></iframe>
                        </div>
                        <div class="thumbnail">
                            <?php echo '<img src="http://view.vzaar.com/'.$vdetail->vzaar_id.'/image" border="0" height="200" width="300">' ; ?>
                            <div class="vzaar_media_image_overlay_hover" vzarid="vzvd-<?php echo $vdetail->vzaar_id ; ?>" data-id="vzvd-<?php echo $vdetail->vzaar_id ; ?>">
                                <a href="#" class="vzaar_video_play"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right_column">
                    <div class="vzaar_info">
                    <?php 
                        $video_date = $vdetail->date ; 
                        $vdate = date_create($video_date);
                        // User Data
                        $user_info = get_userdata($vdetail->user_id);
                    ?>
                        <h3><?php echo ucfirst($vdetail->title) ; ?></h3>
                        <div class="v_detail"><span class="author"><?php echo $user_info->user_login ;?></span> | | <span class="date"><?php echo date_format($vdate, "Y-M-d") ; ?></span></div>
                        <div class="desc"><?php echo $vdetail->description ; ?></div>
                    </div>
                </div>
            </div>
        <?php
        }
	}
	else{
        echo "No Video Found";
    }
}
?>