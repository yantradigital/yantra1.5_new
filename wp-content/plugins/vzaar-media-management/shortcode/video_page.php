<?php
/*
	Shortcode to channel page
*/
function shortcode_video_info($atts)
{
	global $wpdb,$signature;
	$prefix=$wpdb->base_prefix;
	$blog_id = $wpdb->blogid;
	
	$where="where status = '1' group by id desc";
	$querystr = "SELECT * FROM ".$prefix."vzaar_videos $where";
	$videodetails = $wpdb->get_results($querystr, OBJECT);
	
	//$sql = "SELECT * FROM ".$prefix."terms";
	$sql = "SELECT t.* FROM ".$prefix."terms AS t, ".$prefix."term_taxonomy AS tt WHERE t.term_id=tt.term_id AND tt.taxonomy='channels' GROUP BY t.term_id";
	$channels = $wpdb->get_results($sql, OBJECT);
	
?>
<div class="vzaar_videos_section">
    <?php
	if (is_user_logged_in()) 
	{
	?>
    <div class="left_section">
    	<div class="vzaarmedia_result">
        
				<?php	
                foreach($videodetails as $vdetail){
                ?>
                    <div class="video_list">
                        <div class="left_column">
                            <div class="video_info">
                                <div class="video">
                                <iframe allowFullScreen allowTransparency="true" class="vzaar-video-player" frameborder="0" id="vzvd-<?php echo $vdetail->vzaar_id ; ?>" name="vzvd-<?php echo $vdetail->vzaar_id ;?>" src="http://view.vzaar.com/<?php echo $vdetail->vzaar_id ; ?>/player?autoplay=false" title="vzaar video player" type="text/html"></iframe>
                                </div>
                                <div class="thumbnail">
                                    <?php echo '<img src="http://view.vzaar.com/'.$vdetail->vzaar_id.'/image" border="0">' ; ?>
                                    <div class="vzaar_media_image_overlay_hover" vzarid="vzvd-<?php echo $vdetail->vzaar_id ; ?>" data-id="vzvd-<?php echo $vdetail->vzaar_id ; ?>">
                                        <a href="#" class="vzaar_video_play"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right_column">
                            <div class="vzaar_info">
                            <?php 
                                $video_date = $vdetail->date ; 
                                $vdate = date_create($video_date);
                                // User Data
                                $user_info = get_userdata($vdetail->user_id);
                            ?>
                                <h3><?php echo ucfirst($vdetail->title) ; ?></h3>
                                <div class="v_detail"><span class="author"><?php echo $user_info->user_login ;?></span> | | <span class="date"><?php echo date_format($vdate, "Y-M-d") ; ?></span></div>
                                <div class="desc"><?php echo $vdetail->description ; ?></div>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
    
        </div>
   </div>
    <div class="right_section">
        <div class="term_list">
        <?php
		foreach($channels as $chnls)
        {
            echo '<div class="term_section term-'.$chnls->term_id.'"/><a href="#" class="archieve" id="'.$chnls->term_id.'">' . $chnls->name .'</a></div>';
        }
        ?>
        </div>
        <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery(".archieve").click(function(){
				var tid = jQuery(this).attr("id");
				jQuery.post('<?php echo plugins_url() ; ?>/vzaar-media-management/shortcode/channel_page.php?arr=' + tid,function(data) {
					jQuery(".vzaarmedia_result").html(data);
                });
            }); 
        });
        </script>
    </div>
    <?php
	}
	else
	{
	?>
			<div class="show_hide_videos_content">
				<div class="video_content_box">
					<?php	
					foreach($videodetails as $vdetail){
					?>
						<div class="video_list">
							<div class="left_column">
								<div class="video_info">
									<div class="video">
									<iframe allowFullScreen allowTransparency="true" class="vzaar-video-player" frameborder="0" id="vzvd-<?php echo $vdetail->vzaar_id ; ?>" name="vzvd-<?php echo $vdetail->vzaar_id ;?>" src="http://view.vzaar.com/<?php echo $vdetail->vzaar_id ; ?>/player?autoplay=false" title="vzaar video player" type="text/html"></iframe>
									</div>
									<div class="thumbnail">
										<?php echo '<img src="http://view.vzaar.com/'.$vdetail->vzaar_id.'/image" border="0">' ; ?>
										<div class="vzaar_media_image_overlay_hover" vzarid="vzvd-<?php echo $vdetail->vzaar_id ; ?>" data-id="vzvd-<?php echo $vdetail->vzaar_id ; ?>">
											<a href="#" class="vzaar_video_play"></a>
										</div>
									</div>
								</div>
							</div>
							<div class="right_column">
								<div class="vzaar_info">
								<?php 
									$video_date = $vdetail->date ; 
									$vdate = date_create($video_date);
									// User Data
									$user_info = get_userdata($vdetail->user_id);
								?>
									<h3><?php echo ucfirst($vdetail->title) ; ?></h3>
									<div class="v_detail"><span class="author"><?php echo $user_info->user_login ;?></span> | | <span class="date"><?php echo date_format($vdate, "Y-M-d") ; ?></span></div>
									<div class="desc"><?php echo $vdetail->description ; ?></div>
								</div>
							</div>
						</div>
					<?php
					}
					?>
				</div>
				<div class="mem_signup" style="display:none;">
					<?php
					echo "Sign up for premium content and start your 7 days trial for FREE" . "<br/>" ;
					echo "<a href=". 'get_option("home")/login' .">Login</a>" ;
					echo " <a href=". 'get_option("home")/signup' .">Register</a>" ;
					?>
				</div>
			</div>
	<?php
	}
	?>
</div>
 <script src="<?php echo get_template_directory_uri();?>/js/jquery.js"></script>
    <script>
    jQuery(document).ready(function(){
        jQuery(".vzaar_media_image_overlay_hover").click(function(){
            jQuery(this).parent('.thumbnail').hide();
            var iframeid=jQuery(this).attr('data-id');
            var vzarid=jQuery(this).attr('vzarid');
            var iframe = document.getElementById('vzvd-'+vzarid);
            iframe.src = 'http://view.vzaar.com/'+vzarid+'/player?autoplay=true';
            //alert(iframe.src);
            jQuery('#'+iframeid).contentDocument.location.reload(true);
        });
    
		setTimeout(function(){
			jQuery('.mem_signup').show();
			jQuery('.video_content_box').hide();
		}, 9000);	
	});

    </script>
<?php	
}
add_shortcode('video_info', 'shortcode_video_info');