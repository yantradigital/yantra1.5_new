<?php if(!isset($_REQUEST['mem']) )
{ 
	global $wpdb,$signature;
	$prefix=$wpdb->base_prefix;
	$blog_id = $wpdb->blogid;
	
	$totalrec=20;
	if(isset($_REQUEST['pagedid']) && $_REQUEST['pagedid']>1)
	{
		$pageid=$_REQUEST['pagedid'];
		$limitstart=$totalrec*($pageid-1);
	}
	else
	{
		$pageid=1;
		$limitstart=0;
		$limitsend=$totalrec;
	}
	
	
	$where=" order by id desc";
	$querystr = "SELECT * FROM ".$prefix."memberships_level $where limit $limitstart, $totalrec";
	$memlevels = $wpdb->get_results($querystr, OBJECT);
	
	$querystr = "SELECT * FROM ".$prefix."memberships_level $where";
	$totallevels = $wpdb->get_results($querystr, OBJECT);
?>

<link rel="stylesheet" href= "<?php echo plugins_url() ; ?>/yantra_tv_subscription/css/style.css" type='text/css' media='all' />

<?php $url=get_option('home').'/wp-admin/admin.php?page=MembershipSubscription'; ?>
<div class="wrap mainblock">
<h2 class='alignleft'>
	<div class="maintitlebox"><?php echo   __( 'Membership Levels', 'webserve_trdom' ) ; ?></div>
	<a href="<?php _e($url); ?>&mem=addmlevel"><div class="add-new" style="margin: 0.83em 0.5em;">Add New Level</div></a>
</h2>


<div class="clr"></div>
<?php if(isset($_REQUEST['del'])){if($_REQUEST['del']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Deleted successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['add'])){if($_REQUEST['add']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Added successfully.' ); ?></strong></p></div>
<?php }} ?>
<?php if(isset($_REQUEST['update'])){if($_REQUEST['update']=='succ'){ ?>
	<div class="updated"><p><strong><?php _e('Update successfully.' ); ?></strong></p></div>
<?php }} ?>
<div class="clr"></div>


<form name="conatct_form yant_sub" method="post" onSubmit="return check_blank();" action="<?php echo $url; ?>">
<input type="hidden" name="usr" value="filter" />
<div style="clear:both; height:20px;"></div>
	<table class="membership-levels" width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #ccc;">
		<tr>
			<th valign="top" align="left" width="60">&nbsp;<?php _e("ID" ); ?></th>
			<th valign="top" align="left"><?php echo("Name" ); ?></th>
            <th class="billingdetail" valign="top" align="left"><?php echo("Billing Details" ); ?></th>
			<th valign="top" align="left"><?php echo("Expiration" ); ?></th>
            <th valign="top" align="left"><?php echo("Allow Signup" ); ?></th>
            <th valign="top" align="left"><?php echo("Action" ); ?></th>
		</tr>
	<?php 
	$cnt=$limitstart+1; 
	foreach($memlevels as $mlevel)
	{ 
	?>
	 <?php echo '<tr class="' . ($cnt%2 ? 'odd':'even') . '">' ; ?>
		<td valign="top" align="left">&nbsp;<?php _e($cnt); ?></td>
        <td valign="top" align="left" class="level_name">
        <a href="<?php _e($url); ?>&mem=editmlevel&id=<?php _e($mlevel->id); ?>" style="font-weight:bold; font-size:14px !important;">
			<?php echo($mlevel->name); ?>
        </a></td>
		<td class="billingdetail" valign="top"><?php echo($mlevel->description); ?> </td>
        <td valign="top">
		<?php echo($mlevel->expiration_number); ?>
		<?php echo($mlevel->expiration_period); ?></td>
        <td valign="top">
			<?php 
                $alsignup = $mlevel->allow_signups ; 
                if($alsignup == 1)
                {
                    echo 'Yes' ;
                } 
                else
                {
                    echo 'No' ;
                }
            ?>
        </td>
        <td valign="top">
			<a class="btnbox" href="<?php _e($url); ?>&mem=editmlevel&id=<?php _e($mlevel->id); ?>"> Edit</a>&nbsp;&nbsp;
			<a class="btnbox" href="javascript:if(confirm('Please confirm that you would like to delete this case?')) {window.location='<?php _e($url); ?>&mem=deletemlevel&id=<?php _e($mlevel->id); ?>';}">Delete</a>
		</td>
	  </tr>
	  <?php $cnt++; } ?>
	</table>
</form>
<?php if(count($totallevels)>$totalrec){ ?>
<div style="float:left; margin-top:10px;" class="pagination">

<?php if($pageid>1){ ?><a href="<?php _e($url); ?>" title="First" class="fl"><img src="<?php echo get_option('home');?>/wp-content/plugins/yantra_tv_subscription/images/first.png" alt="First" title="First" /></a><?php } ?>
    <?php $totalpages=ceil(count($totallevels)/$totalrec);
			
			$previous = $pageid-1;
			if($previous>0)
			{
				?>
				<a class="fl" href="<?php _e($url.'&amp;pagedid='.$previous);?>"><img src="<?php echo get_option('home');?>/wp-content/plugins/yantra_tv_subscription/images/previous.png" alt="previous" title="previous" /></a>
				<?php
			}
			?>
            <div class="fl ml5 mr10">Page Number:</div>
            <div class="fl mr5">
            	<script type="text/javascript">
				//<![CDATA[
					jQuery(document).ready( function(){
						jQuery('#paginate').live('change', function(){
							var pagedid=jQuery(this).val();
							window.location='<?php _e($url); ?>&pagedid='+pagedid;
						});
					})
					//]]>
				</script>
                <select style="float:left;" id="paginate" name="pagedid">
                <?php for($k=1;$k<=$totalpages;$k++){ ?>
                    <option value="<?php _e($k); ?>" <?php if($k==$pageid){ _e('selected="selected"');}?>><?php _e($k); ?></option>
                <?php } ?>
                </select>
           	</div>
			<?php
				
			
			$next = $pageid+1;
			if($totalpages>=$next)
			{
				?>
				<a class="fl" href="<?php _e($url.'&amp;pagedid='.$next);?>"><img src="<?php echo get_option('home');?>/wp-content/plugins/yantra_tv_subscription/images/next.png" alt="next" title="next" /></a>
				<?php
			}
     ?>
     <?php if($totalpages>$pageid){ ?><a href="<?php _e($url.'&amp;pagedid='.$totalpages); ?>" title="Last" class="fl"><img src="<?php echo get_option('home');?>/wp-content/plugins/yantra_tv_subscription/images/last.png" alt="Last" title="Last" /></a><?php } ?>

</div>
<?php } ?>
</div>

<?php } ?>