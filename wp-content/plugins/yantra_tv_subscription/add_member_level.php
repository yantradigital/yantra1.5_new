<?php 
global $wpdb,$signature;
$prefix=$wpdb->base_prefix;
$error=array();

if(isset($_POST['registration']))
{
	$name = $_POST['name'];
	$description = $_POST['description'];
	$ipayment = $_POST['initial_payment'];
	$bamount = $_POST['billing_amount'];
	$cycle_number = $_POST['cycle_number'] ;
	$cycle_period = $_POST['cycle_period'] ;
	$tlimit = $_POST['trial_limit'] ;
	$non_payment = $_POST['non_payment'] ;
	
	if(count($error)<=0)
	{
		$sql="INSERT INTO `".$prefix."memberships_level` (`name`, `description`, `initial_payment`, `cycle_number`, `cycle_period`, `billing_amount`, `trial_limit`, `expiration_number`, `expiration_period`, `non_payment`) VALUES ('$name', '$description', '$ipayment', '$cycle_number', '$cycle_period', '$bamount', '$tlimit', '$cycle_number', '$cycle_period', '$non_payment')";
		
		$result = $wpdb->query( $sql );
		if($result==1)
		{
			$url=get_option('home').'/wp-admin/admin.php?page=MembershipSubscription&add=succ';
			echo"<script>window.location='".$url."'</script>";
		}
	}
}

?>
<link rel="stylesheet" href= "<?php echo plugins_url() ; ?>/yantra_tv_subscription/css/style.css" type='text/css' media='all' />
<div class="yant_sub">
<h2>Add New Membership Level</h2>
	<div class="profile donotshowerror">
    	<?php if(count($error)>0)
		  { ?>
		<div class="tabletitle"><span class="error">Error</span></div>
		<table width="700" class="from_main" border="0" cellpadding="0" cellspacing="0">
		  <?php 
		   
			for($i=0;$i<count($error);$i++)
			{
				?>
			 <tr>
				<td align="left" valign="top" class="name"><span class="error"><?php echo $error[$i]; ?></span></td>
			 </tr>
	<?php	} ?>
		</table>
		<div class="clear"></div>
	 <?php } ?>
        <div class="right donotshowerror">
        	<form action="" method="post" name="register_spcialist" id="register_spcialist" >
                <div class="e-mail">
                    <div class="adress">Name : </div>
                    <div class="field"><input type="text" name="name" /></div>
                </div>
                
                <div class="e-mail">
                    <div class="adress">Description : </div>
                    <div class="field">
                    <?php 
					$settings = array(
						'editor_height' => 425, // In pixels, takes precedence and has no default value
						'textarea_rows' => 20,  // Has no visible effect if editor_height is set, default is 20
					);
					the_editor($description, 'description', $settings) 
					?></div>
                </div>
               
                <h3>Other Setting</h3>
                <hr/>
                <div class="e-mail">
                    <div class="adress">Non Payment : </div>
                    <div class="field"><input type="checkbox" name="non_payment" value="1" /> Non Payment
                    </div>
                </div>
                
                <div class="e-mail">
                    <div class="adress">Trial Period (days) : </div>
                    <div class="field"><input type="text" name="trial_limit" /></div>
                </div>
                
                <h3>Billing Detail</h3>
                <hr/>
                <div class="e-mail">
                    <div class="adress">Initial Payment ($) : </div>
                    <div class="field"><input type="text" name="initial_payment"/></div>
                </div>
                
                <div class="e-mail">
                    <div class="adress">Billing Amount ($) : </div>
                    <div class="field"><input type="text" name="billing_amount" /></div>
                </div>
                
                <div class="e-mail">
               		<div class="adress">Expiration Period : </div>
                    <div class="field">
                    <div class="field_first_half"><input id="cycle_number" name="cycle_number" type="text" size="10" value="0"></div>
                    <div class="field_second_half">
                    <select id="cycle_period" name="cycle_period">
						<option value="D">Day(s)</option>
                        <option value="W">Week(s)</option>
                        <option value="M">Month(s)</option>
                        <option value="Y">Year(s)</option>						
                    </select>
                    </div>
                    </div>
               </div>
               
               <div class="clear"></div>
                <div class="e-mail">
                    <div class="adress">&nbsp;&nbsp;</div>
                    <div class="field" style="margin-top:10px;">
                        <div class="green-submit-btn">
                        	<input type="submit" name="registration" value="Save Level" class="registration_btn"/> 
                            <input onclick="return backtolist()" type="button" name="back" value="Cancel" title="Back" />
                       </div>
                    </div>
                </div>
            </form>
        </div>
     </div>
<div class="clear"></div>
</div>
<script type="text/javascript">
function backtolist()
{
	window.location='<?php echo get_option('home').'/wp-admin/admin.php?page=MembershipSubscription'; ?>';
}
/*jQuery(document).ready(function(){
   jQuery('input[name="recurring"]').change(function(){
       if(this.checked)
	   {
	     jQuery(".recurring_info").show();
	   }
	   else
	   {
	   	  jQuery(".recurring_info").hide();
	   }
    });
});*/
</script>