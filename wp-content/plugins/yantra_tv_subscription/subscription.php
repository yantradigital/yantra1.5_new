<?php 
@session_start();

/*

Plugin Name: Membership Subscription

Plugin URI: http://www.vantagewebtech.com

Description: This is membership subscription plugin

Author: Priyaka Chhabra

Version: 10.06.14

Author URI: http://www.vantagewebtech.com

*/
//*************** Admin function ***************
//this function is used for checking login details

// Add Metabox

require_once("metaboxes.php");
//add_action ('admin_head','cssfile')


if(!isset($_REQUEST['Membership_level']) || ($_REQUEST['mem']=='Membership_level') )
{
	function Membership_level() {
		include('list_member_level.php');
	}
}
if(!isset($_REQUEST['Membership_list']) || ($_REQUEST['mem']=='Membership_list') )
{
	function Membership_list() {
		include('list_members.php');
	}
}
if(isset($_REQUEST['mem']))
{
	$page=$_REQUEST['mem'];
	switch($page)
	{
		case 'addmlevel':
		function includesetting_files() {
			include('add_member_level.php');
		}
		break;
		case 'editmlevel':
		function includesetting_files() {
			include('edit_member_level.php');
		}
		break;
		case 'deletemlevel':
		function includesetting_files() {
			include('delete_member_level.php');
		}
		break;
		case 'cancelmembers':
		function includesetting_files() {
			include('cancel_members.php');
		}
		break;
	}
	function managePackage_admin_actions2() {
		add_menu_page("MembershipSubscription", "Membership", 1, "MembershipSubscription", "includesetting_files");
		add_submenu_page( 'MembershipSubscription', 'Membership Level', 'Membership Level', '1',  'MembershipSubscription', 'Membership_level' );
		add_submenu_page( 'MembershipSubscription', 'Members List', 'Members List', '1',  'MembersList', 'includesetting_files' );
	}
	add_action('admin_menu', 'managePackage_admin_actions2');
}



function managesubscription_admin_actions() 
{	
	add_menu_page("MembershipSubscription", "Membership", 1, "MembershipSubscription", "Membership_level");
	add_submenu_page( 'MembershipSubscription', 'Membership Level', 'Membership Level', '1',  'MembershipSubscription', 'Membership_level' );
	add_submenu_page( 'MembershipSubscription', 'Members List', 'Members List', '1',  'MembersList', 'Membership_list' );
}

if(!isset($_REQUEST['mem']) )
{
	add_action('admin_menu', 'managesubscription_admin_actions');
}

// Membership Level Data
function leveldetail($id='', $cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if($id!='')
	{
		$cond.=" and id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."memberships_level where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}

// User Data
function usersdetail($id='', $cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if($id!='')
	{
		$cond.=" and id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."memberships_users where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}

// Order Data
function orderdetail($id='', $cnd='')
{
	global $wpdb;
	$prefix=$wpdb->base_prefix;
	$cond='';
	if($id!='')
	{
		$cond.=" and id='$id'";
	}
	if($cnd!='')
	{
		$cond.=" $cnd";
	}
	$querystr = "SELECT * FROM ".$prefix."membership_orders where id!='' $cond";
	$data = $wpdb->get_results($querystr, OBJECT);
	return $data;
}


require_once("login-shortcodes/account.php");  // [membership_account] shortcode to show level
require_once("login-shortcodes/membership-cancel.php");  // [membership_cancel] shortcode to show level
//require_once("login-shortcodes/logout.php");  // [membership_logout] shortcode to show level

require_once("shortcodes/level.php");			//[membership_level] shortcode to show level
require_once("shortcodes/checkout.php");		//[membership_checkout] shortcode to show checkout page
require_once("shortcodes/signup.php");		    //[membership_signup] shortcode to show signup
require_once("shortcodes/confirmation.php");	//[membership_confirmation] shortcode to show confirmation page
require_once("shortcodes/payment.php");	        //[membership_payment] shortcode to show payment page
require_once("shortcodes/thankyou.php");	    //[membership_thankyou] shortcode to show payment page
require_once("shortcodes/login.php");	        //[membership_login] shortcode to show payment page
require_once("shortcodes/paypal.php");	        //[membership_paypal] shortcode to show payment page



function subscription_install() {
   global $wpdb;
   //global $product_db_version;


$sql = "CREATE TABLE `".$wpdb->prefix."memberships_level` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		  `name` varchar(255) NOT NULL,
		  `description` longtext NOT NULL,
		  `initial_payment` decimal(10,2) NOT NULL DEFAULT '0.00',
		  `cycle_number` int(10) unsigned NOT NULL,
		  `cycle_period` enum('D', 'W', 'M', 'Y') NOT NULL,
		  `billing_limit` int(11) NOT NULL DEFAULT '0',
		  `billing_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
		  `trial_limit` int(11) NOT NULL DEFAULT '0',
		  `allow_signups` tinyint(4) NOT NULL DEFAULT '1',
		  `expiration_number` int(10) unsigned NOT NULL,
		  `expiration_period` enum('D', 'W', 'M', 'Y') NOT NULL,
		  `non_payment` int(11) unsigned NOT NULL,
		  PRIMARY KEY (`id`)
)";


$sql1 = "CREATE TABLE `".$wpdb->prefix."memberships_users` (
           `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		   `user_id` int(11) unsigned NOT NULL,
		   `membership_id` int(11) unsigned NOT NULL,
		   `code_id` int(11) unsigned NOT NULL,
		   `initial_payment` decimal(10,2) NOT NULL,
		   `billing_amount` decimal(10,2) NOT NULL,
		   `billing_limit` int(11) NOT NULL,
		   `trial_limit` int(11) NOT NULL,
		   `status` varchar(20) NOT NULL DEFAULT 'active',
		   `startdate` datetime NOT NULL,
		   `enddate` datetime DEFAULT NULL,
		   `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		   PRIMARY KEY (`id`),
		   KEY `membership_id` (`membership_id`),
		   KEY `modified` (`modified`),
		   KEY `enddate` (`enddate`),
		   KEY `user_id` (`user_id`),
		   KEY `status` (`status`)
)";

$sql2 = "CREATE TABLE `".$wpdb->prefix."memberships_pages` (
          `membership_id` int(11) unsigned NOT NULL,
		  `page_id` int(11) unsigned NOT NULL,
		  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  UNIQUE KEY `category_membership` (`page_id`,`membership_id`),
		  UNIQUE KEY `membership_page` (`membership_id`,`page_id`)
)";

$sql3 = "CREATE TABLE `".$wpdb->prefix."membership_orders` (
		  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		  `code` varchar(10) NOT NULL,
		  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
		  `membership_id` int(11) unsigned NOT NULL DEFAULT '0',
		  `billing_name` varchar(128) NOT NULL DEFAULT '',
		  `billing_street` varchar(128) NOT NULL DEFAULT '',
		  `billing_city` varchar(128) NOT NULL DEFAULT '',
		  `billing_state` varchar(32) NOT NULL DEFAULT '',
		  `billing_zip` varchar(16) NOT NULL DEFAULT '',
		  `billing_country` varchar(128) NOT NULL,
		  `billing_phone` varchar(32) NOT NULL,
		  `total` varchar(16) NOT NULL DEFAULT '',
		  `payment_type` varchar(64) NOT NULL DEFAULT '',
		  `cardtype` varchar(32) NOT NULL DEFAULT '',
		  `accountnumber` varchar(32) NOT NULL DEFAULT '',
		  `expirationmonth` char(2) NOT NULL DEFAULT '',
		  `expirationyear` varchar(4) NOT NULL DEFAULT '',
		  `status` varchar(32) NOT NULL DEFAULT '',
		  `payment_transaction_id` varchar(64) NOT NULL,
		  `subscription_transaction_id` varchar(32) NOT NULL,
		  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `code` (`code`),
		  KEY `session_id` (`session_id`),
		  KEY `user_id` (`user_id`),
		  KEY `membership_id` (`membership_id`),
		  KEY `status` (`status`),
		  KEY `timestamp` (`timestamp`),
		  KEY `payment_transaction_id` (`payment_transaction_id`),
		  KEY `subscription_transaction_id` (`subscription_transaction_id`),
)";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
   dbDelta($sql);
   dbDelta($sql1);
   dbDelta($sql2);
   dbDelta($sql3);
}
register_activation_hook(__FILE__,'subscription_install');



function subs_login_redirect( $url, $request, $user ){
if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) 
{
	if( $user->has_cap( 'administrator' ) ) {
		$url = admin_url();
	} 
	else
	{
		$url = home_url('/videos/');
	}
}
	return $url;
}
add_filter('login_redirect', 'subs_login_redirect', 10, 3 );
?>