<?php
/*
	Require Membership Meta Box
*/
// Add the Memberships Meta Boxes
add_action( 'add_meta_boxes', 'mem_page_meta_wrapper' );

function mem_page_meta_wrapper() {
	add_meta_box('mem_page_meta', 'Require Membership', 'mem_page_meta', 'videos', 'side');
}

// The Membership Page Metabox
function mem_page_meta() {
	global $membership_levels, $post, $wpdb;
	$membership_levels = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."memberships_level", OBJECT );
	
	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="mem_meta_noncename" id="mem_meta_noncename" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	
	// Get the membership level data if its already been entered
	$m_levels = get_post_meta($post->ID, 'membership_level', true);
	$memlevels=get_post_meta($post->ID, 'membership_level', true);
	if(empty($memlevels)){$memlevels=array();}
	// Echo out the field
	foreach($membership_levels as $level)
	{
?>
		<input id="membership-level-<?php echo $level->id?>" type="checkbox"<?php if(in_array($level->id,$memlevels)){echo' checked="checked"';} ?> name="membership_level[]" value="<?php echo $level->id?>" />
<?php
		echo $level->name . '<br/>';
	}
}

// Save the Metabox Data
function mem_save_page_meta($post_id, $post) {
	global $wpdb;
	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( !wp_verify_nonce( $_POST['mem_meta_noncename'], plugin_basename(__FILE__) )) {
		return $post->ID;
	}

	// Is the user allowed to edit the post or page?
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	// OK, we're authenticated: we need to find and save the data
	// We'll put it into an array to make it easier to loop though.
	
	// Add values of $events_meta as custom fields
		if( $post->post_type == 'revision' ) return;
		$key='membership_level';
		if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
			update_post_meta($post->ID, $key, $_POST['membership_level']);
			$wpdb->query("DELETE from ".$wpdb->prefix."memberships_pages WHERE page_id =".$post->ID);
			foreach($_POST['membership_level'] as $k => $v)
			{
				$wpdb->query("INSERT INTO ".$wpdb->prefix."memberships_pages(membership_id, page_id) VALUES('" . $v . "', '" . $post->ID . "')") ;
				//echo $wpdb->query( );
			}
		} else { // If the custom field doesn't have a value
			add_post_meta($post->ID, $key, $_POST['membership_level']);
			foreach($_POST['membership_level'] as $k => $v)
			{
				$wpdb->query("INSERT INTO ".$wpdb->prefix."memberships_pages(membership_id, page_id) VALUES('" . $v . "', '" . $post->ID . "')" );
			}
		}
}
add_action('save_post', 'mem_save_page_meta', 1, 2); // save the custom fields