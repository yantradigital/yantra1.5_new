<?php

	global $wpdb,$signature,$cancel_message;
	$prefix=$wpdb->base_prefix;
	$blog_id = $wpdb->blogid;
	
	$error=array();
	
	if(isset($_POST['member_status']))
	{
		//print_r($_POST);
		foreach($_POST['check'] as $k => $v)
		{
			$sitename = get_option('blogname');	
			//$to     = get_option('admin_email');	
			$subject  = $sitename.' :: Get a quote';	
			$from     = get_option('admin_email') ;
			$fromname = $sitename;
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\nFrom: $fromname <$from>\r\nReply-To: $from";
			
			$message = "Dear User<br /><br />";
			$message.= $cancel_message;
			$message.="The following user has filled the quote form. Further details are given below:<br/><br/>";
			$message.="";
			$message.='<div style="clear:both;margin-top:20px;"></div>Thanks<br/>';
			
			if($sent_message = wp_mail($v, $subject, $message, $headers))
			{
				echo 'Mail send to '.$v.'</br>';
			}
		}
	}
	
	$totalrec=20;
	if(isset($_REQUEST['pagedid']) && $_REQUEST['pagedid']>1)
	{
		$pageid=$_REQUEST['pagedid'];
		$limitstart=$totalrec*($pageid-1);
	}
	else
	{
		$pageid=1;
		$limitstart=0;
		$limitsend=$totalrec;
	}
	
	
	$where="where status = 'cancelled'";
	$querystr = "SELECT * FROM ".$prefix."memberships_users $where limit $limitstart, $totalrec";
	$cancelledusers = $wpdb->get_results($querystr, OBJECT);
	
	$querystr = "SELECT * FROM ".$prefix."memberships_users $where";
	$totalusers = $wpdb->get_results($querystr, OBJECT);
?>

<link rel="stylesheet" href= "<?php echo plugins_url() ; ?>/yantra_tv_subscription/css/style.css" type='text/css' media='all' />

<?php $url=get_option('home').'/wp-admin/admin.php?page=MembersList'; ?>
<div class="yant_sub">
<?php echo "<h2>" . __( 'Cancelled Members List', 'webserve_trdom' ) . "</h2>"; ?>
<div class="clr"></div>



<form name="cancel_form" method="post" action="">
<input type="hidden" name="mem" value="cancelmembers" />
<div style="clear:both; height:20px;"></div>
	<table class="membership-levels" width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #ccc;">
		<tr>
			<th valign="top" align="left" width="60"><input type='checkbox' onchange='checkAll(this)' name='chk'/></th>
            <th valign="top" align="left" width="60" style="border-left:1px solid #ccc;">&nbsp;<?php _e("ID" ); ?></th>
			<th valign="top" align="left" style="border-left:1px solid #ccc;"><?php echo("User Name" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php echo("First Name" ); ?></th>
			<th valign="top" align="left" style="border-left:1px solid #ccc;"><?php echo("Last Name" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php echo("Email" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php echo("Billing Address" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php echo("Membership" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php echo("Fee" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php echo("Joined" ); ?></th>
            <th valign="top" align="left" style="border-left:1px solid #ccc;"><?php echo("Expires" ); ?></th>
		</tr>
	<?php 
	$cnt=$limitstart+1; 
	foreach($cancelledusers as $cusers)
	{ 
		$userdata = get_userdata($cusers->user_id);
		$join_date = $userdata->user_registered;
		$memid = leveldetail($cusers->membership_id) ;		
	?>
	  <?php echo '<tr class="' . ($cnt%2 ? 'odd':'even') . '">' ; ?>
      	<td valign="top" align="left" style="border-top:1px solid #ccc;"><input type="checkbox" name="send_email"/></td>
		<td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
        <input type="hidden" value="<?php echo ($cusers->id) ;?>" name="id[]"/>&nbsp;<?php _e($cnt); ?></td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
		<?php echo($userdata->user_nicename); ?><input type="hidden" value="<?php echo ($userdata->user_nicename) ;?>" name="username[]"/></td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php echo($userdata->first_name); ?></td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php echo($userdata->last_name); ?></td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;">
		<input type="hidden" value="<?php echo ($userdata->user_email) ;?>" name="check[]"/>
		<?php echo($userdata->user_email); ?></td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php echo($user->user_id); ?></td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php echo($memid[0]->name); ?></td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php echo($cusers->billing_amount); ?></td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php echo(date('Y-m-d', strtotime($join_date))); ?></td>
        <td valign="top" align="left" style="border-top:1px solid #ccc; border-left:1px solid #ccc;"><?php echo($cusers->enddate); ?></td>
        <div class="status_field">
		<input type="submit" class="member_status" value="Submit" name="member_status"/>
		</div>
	  </tr>
	  <?php $cnt++; } ?>
	</table>
</form>
<?php if(count($totallevels)>$totalrec){ ?>
<div style="float:left; margin-top:10px;" class="pagination">

<?php if($pageid>1){ ?><a href="<?php _e($url); ?>" title="First" class="fl"><img src="<?php echo get_option('home');?>/wp-content/plugins/yantra_tv_subscription/images/first.png" alt="First" title="First" /></a><?php } ?>
    <?php $totalpages=ceil(count($totallevels)/$totalrec);
			
			$previous = $pageid-1;
			if($previous>0)
			{
				?>
				<a class="fl" href="<?php _e($url.'&amp;pagedid='.$previous);?>"><img src="<?php echo get_option('home');?>/wp-content/plugins/yantra_tv_subscription/images/previous.png" alt="previous" title="previous" /></a>
				<?php
			}
			?>
            <div class="fl ml5 mr10">Page Number:</div>
            <div class="fl mr5">
            	<script type="text/javascript">
				//<![CDATA[
					jQuery(document).ready( function(){
						jQuery('#paginate').live('change', function(){
							var pagedid=jQuery(this).val();
							window.location='<?php _e($url); ?>&pagedid='+pagedid;
						});
					})
					//]]>
				</script>
                <select style="float:left;" id="paginate" name="pagedid">
                <?php for($k=1;$k<=$totalpages;$k++){ ?>
                    <option value="<?php _e($k); ?>" <?php if($k==$pageid){ _e('selected="selected"');}?>><?php _e($k); ?></option>
                <?php } ?>
                </select>
           	</div>
			<?php
				
			
			$next = $pageid+1;
			if($totalpages>=$next)
			{
				?>
				<a class="fl" href="<?php _e($url.'&amp;pagedid='.$next);?>"><img src="<?php echo get_option('home');?>/wp-content/plugins/yantra_tv_subscription/images/next.png" alt="next" title="next" /></a>
				<?php
			}
     ?>
     <?php if($totalpages>$pageid){ ?><a href="<?php _e($url.'&amp;pagedid='.$totalpages); ?>" title="Last" class="fl"><img src="<?php echo get_option('home');?>/wp-content/plugins/yantra_tv_subscription/images/last.png" alt="Last" title="Last" /></a><?php } ?>

</div>
<?php } ?>
</div>
<script type="text/javascript" language="javascript">// <![CDATA[
function checkAll(ele) {
	var checkboxes = document.getElementsByTagName('input');
	if (ele.checked) {
		 for (var i = 0; i < checkboxes.length; i++) {
			 if (checkboxes[i].type == 'checkbox') {
				 checkboxes[i].checked = true;
			 }
		 }
	} else {
		 for (var i = 0; i < checkboxes.length; i++) {
			 console.log(i)
			 if (checkboxes[i].type == 'checkbox') {
				 checkboxes[i].checked = false;
			 }
		 }
	}
}
</script>