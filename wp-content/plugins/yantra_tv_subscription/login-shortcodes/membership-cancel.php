<?php
/* Shortcode to cancel*/
function shortcode_cancel_member($atts)
{
	if (is_user_logged_in()) 
	{
		global $post,$wpdb,$signature;
		$mlevel = $_REQUEST['level'];
		$userid = wp_get_current_user()->ID;
		//print_r (wp_get_current_user()) ;
		$mem_detail = usersdetail($userid);
		$mem_id = $mem_detail[0]->membership_id ;
		$level_detail = leveldetail($mem_id) ;
		$url=get_option('home');
		$sendemail=true;
		if(isset($_POST['submit']))
		{
			$user_id = $_POST['user_id'] ;
			$membership_id = $_POST['membership_id'] ;
			
			$tablename=$wpdb->prefix.'memberships_users';
			
			$sql = "UPDATE " . $tablename . " SET status = 'cancelled' where `user_id` = ".$user_id." and `membership_id` = ".$membership_id ;
			$result = $wpdb->query($sql);
			$last_id=$wpdb->insert_id;
			//wp_logout_url( $redirect ); 
			if($sendemail)
			{
				
				// $sitename = get_option('blogname');
				$sitename = 'Yantratv payment subscription' ;
				$to       = "priyanka.vantage123@gmail.com";	
				//print_r ($to); 
				//$to     = get_option('admin_email');	
				$subject  = $sitename ;	
				$from     = get_option('admin_email') ;
				$fromname = $sitename;
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\nFrom: $fromname <$from>\r\nReply-To: $from";
				
				$message="Dear ".wp_get_current_user()->display_name."<br /><br />";
				$message.="Your account has been cancelled.<br/><br/>";
				
				/*$message.="Your account e-mail:: ". $email_id . "<br/>";
				$message.="Your account username: ". $user_name . "<br/>";
				$message.="Your account password: ". $password . "<br/><br/>";*/
					
				$message.='<div style="clear:both;margin-top:20px;"></div>Best Regards!<br/>';
				//echo 'test' ;	
				$sent_message = wp_mail($to, $subject, $message, $headers);
			}
			wp_logout();
			$url = get_option('home').'/videos';
			echo "<script>window.location='".$url."'</script>";
		}
		$data='' ;
		$data.='<div id="mem_cancel">		
				<p>Are you sure you want to cancel your '.$level_detail[0]->name.' membership?</p>
								
			<div class="mem_actionlinks">
				<form action="" method="post">
				<input type="hidden" value="'.$userid.'" name="user_id"/>
				<input type="hidden" value="'.$mlevel.'" name="membership_id"/>
				<input type="submit" name="submit" class="mem_btn mem_yeslink yeslink btnbox" value="Yes, cancel my account"/>
				</form>
				<a class="mem_btn mem_cancel mem_nolink nolink btnbox" href="../account">No, keep my account</a>
			</div>
					
		</div>';
		return $data ;
	}
}
add_shortcode('membership_cancel', 'shortcode_cancel_member');