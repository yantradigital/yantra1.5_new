<?php
/* Shortcode to account information*/
function shortcode_account_page($atts)
{
	if (is_user_logged_in()) 
	{
	global $post,$wpdb,$signature;
	$userid = wp_get_current_user()->ID;
	$user_detail = wp_get_current_user();
	$mem_detail = usersdetail($userid);
	$mem_id = $mem_detail[0]->membership_id ;
	$levelid = leveldetail($mem_id);
	//print_r ($levelid);
	// Login User Account Detail
	/*$where = "WHERE user_id = " . $userid ;
	$sql = "SELECT * FROM ".$prefix."memberships_users $where" ;
	$memresult = $wpdb->get_results($sql, OBJECT);*/
	
	
	//print_r ($userid) ;
	$data.='<div class="yant_sub">';
		$data.='<h2 class="account-heading">Account Summary</h2>';
		$data.='<div class="account_detail">';
		$data.='<div id="mem_account-profile" class="mem_box">'	;
			$data.='<div class="pmpro-profile-padding">';
			$data.='<h3 style="padding-left:0px !important;">Membership Level:&nbsp;<strong>'.ucfirst($levelid[0]->name).'</strong></h3>' ;
			$data.='<h4 style="margin-bottom:5px;">My Details:</h4>';
			$data.='<ul>';
			$data.='<li><strong>Username:</strong> '.wp_get_current_user()->user_login.'</li>' ;
			$data.='<li><strong>Name:</strong> '.wp_get_current_user()->display_name.'</li>' ;
			$data.='<li><strong>Email:</strong> '.wp_get_current_user()->user_email.'</li>' ;
			$data.='</ul>';
			$data.='</div>';
		$data.='</div>' ;
		$data.='</div>' ;
		
		$data.='<h2 class="account-heading">My Memberships</h2>';
		$data.='<div class="account_detail membership">';
		$data.='<table width="100%" cellpadding="0" cellspacing="0" border="0">' ;
		$data.='<thead>';
			$data.='<tr>';
					$data.='<th>Level</th>';
					$data.='<th>Billing</th>';
					$data.='<th>Expiration</th>';
			$data.='</tr>';
		$data.='</thead>';
		$data.='<tbody>';
			$data.='<tr>' ;
				$data.='<td class="mem_account-membership-levelname">'; 
					$data.=''. $levelid[0]->name ;							
					$data.='<div class="mem_actionlinks">';
						$data.='<a href="level" class="btnbox">Change</a>' ;
						$data.=' <a href="membership-cancel/?level='.$levelid[0]->id.'" class="btnbox">Cancel</a>' ;
					$data.='</div>';
				$data.='</td>';
				$data.='<td class="mem_account-membership-levelfee">';
					$data.='<p>At the end of your free 7 days trial, you authorise us to charge you for the plan above. After the free trial period the subscription renews automatically until cancelled.<br>If you cancel during your free trial period, you wont be charged.</p>';
				$data.='</td>' ;
				$data.='<td class="mem_account-membership-expiration">';
					$data.='---	';					
				$data.='</td>';
			$data.='</tr>' ;
		$data.='</tbody>';
	$data.='</table>' ;
	$data.='<div class="mem_actionlinks">';
		$data.='<a href="level">View all Membership Options</a>';
	$data.='</div>';
	$data.='</div>';
	
	return $data;
	}
}
add_shortcode('membership_account', 'shortcode_account_page');
