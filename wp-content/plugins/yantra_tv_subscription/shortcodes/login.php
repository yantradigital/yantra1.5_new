<?php
/*Login form shortcode*/
function login_shortcode($atts)
{	
	global $post,$wpdb,$signature;
	if (!is_user_logged_in()) 
	{
		$data='';
		$data.='<div class="yant_sub">' ;
		$data.= '' . wp_login_form() ;
		$data.='</div>';
		
		return $data;
	}
	else
	{
		$url = get_option('home').'/videos';
		echo "<script>window.location='".$url."'</script>";
	}
}
add_shortcode("membership_login", "login_shortcode");