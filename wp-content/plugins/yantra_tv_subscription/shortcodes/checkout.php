<?php
//checkout shortcode separated out so we can have multiple checkout pages

function checkout_shortcode($atts)
{	
global $post,$wpdb,$signature;

$mlevel = $_REQUEST['level'];
$levelid = leveldetail($mlevel);
if(isset($_POST['submit']))
{
	$_SESSION['MEM']['mlevel'] = $_POST['level'];
	$memlevel_id = $_SESSION['MEM']['mlevel'] ;
	$url=get_option('home').'/signup';
	echo"<script>window.location='".$url."'</script>";
}
$data='';
	$data.='<div class="yant_sub">';
	$data.='<div id="mem_level-'.$mlevel.'">' ;
	$data.='<form id="mem_form" class="mem_form" action="" method="post">' ;
	$data.='<input type="hidden" id="level" name="level" value="'.$mlevel.'">';
	$data.='<table id="mem_pricing_fields" class="mem_checkout" width="100%" cellpadding="0" cellspacing="0" border="0">
	<thead>
		<tr>
			<th>
				<span class="mem_thead-name">Membership Level</span>
				<span class="mem_thead-msg"><a href="../level">change</a></span></th>
		</tr>
	</thead>';
	$data.='<tbody>
			<tr><td><p>You have selected the <strong>'.$levelid[0]->name.'</strong> membership level.</p>
				<div id="mem_level_cost"><p>The price for membership is <strong>$'.$levelid[0]->billing_amount.'</strong> now. </p>
					<p>Membership expires after '.$levelid[0]->expiration_number." ".$levelid[0]->expiration_period.'.</p>
				</div>
			</td></tr>
		</tbody>';
	$data.='</table>' ;
	$data.='<div class="mem_submit">';
	$data.='<input type="submit" name="submit" value="Submit" class="mem_submit_btn btnbox " />';
	$data.='<span id="mem_processing_message" style="display:none;">Processing...</span>';
	$data.='</div>';
	$data.='</form>' ;
	$data.='</div>' ;
	$data.='</div>' ;
	return $data;
	
}
add_shortcode("membership_checkout", "checkout_shortcode");