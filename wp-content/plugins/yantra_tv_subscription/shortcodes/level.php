<?php
/*
	Shortcode to level information
*/
function shortcode_level_info($atts)
{
	global $wpdb;
	$data='';
	$data.='<div class="yant_sub">';
		$mem_levels = leveldetail('', 'order by id asc');
		if(count($mem_levels)>0)
		{
			$data.='<table>
					<thead>
					<tr>
						<th>Level</th>
						<th class="pricebox">Price</th>	
						<th>&nbsp;</th>
					 </tr></thead>';
			$data.='<tbody>' ;
			foreach($mem_levels as $mem_level)
			{
			   $data.='<tr>' ;
			   $data.='<td>'.$mem_level->name.'</td>' ;
			   $trial_limit = $mem_level->trial_limit ;
			   if($trial_limit>0)
			   {
			   		$data.='<td class="pricebox"><strong>$'.$mem_level->initial_payment.'</strong> now. After '.$mem_level->trial_limit.' days the subscription <strong>$'.$mem_level->billing_amount.'</strong> Membership expires after '.$mem_level->expiration_number . " " . $mem_level->expiration_period.'.</td>' ;
			   }
			   else
			   {	
			   		$data.='<td class="pricebox"><strong>$'.$mem_level->billing_amount.'</strong> now. Membership expires after '.$mem_level->expiration_number . " " . $mem_level->expiration_period.'.</td>' ;
			   }
			   $data.='<td><a class="btnbox slct" href="'.get_option('home').'/confirmation/?level=' . $mem_level->id.'" class="mem_select_btn">Select</a></td>' ;
			   $data.='</tr>' ;
			}
			$data.='</tbody>' ;
			$data.='</tr></table>';
		}
		$data.='</div>';
	return $data;
}
add_shortcode('membership_level', 'shortcode_level_info');