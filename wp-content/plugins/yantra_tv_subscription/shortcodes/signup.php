<?php
/*Signup form shortcode*/
function signup_shortcode($atts)
{	
	global $post,$wpdb,$signature;
	/*$memlevel_id = $_SESSION['MEM']['mlevel'] ;
	$levelid = leveldetail($memlevel_id);
	$trial_limit = $levelid[0]->trial_limit ;*/
	
	$url=get_option('home');
	//$paypal_id='vantag_1298611446_biz@gmail.com'; // Business email ID
	if(isset($_POST['registration']))
	{
		$_SESSION['MEM']['user_name'] = $_POST['user_name'];
		$_SESSION['MEM']['emailid'] = $_POST['emailid'];
		$_SESSION['MEM']['first_name'] = $_POST['first_name'];
		$_SESSION['MEM']['last_name'] = $_POST['last_name'];
		$_SESSION['MEM']['password'] = $_POST['password'];
		$_SESSION['MEM']['initial_amount'] = $_POST['initial_amount'];
		$_SESSION['MEM']['billing_amount'] = $_POST['billing_amount'];
		$_SESSION['MEM']['membership_id'] = $_POST['membership_id'];
		$_SESSION['MEM']['non_payment'] = $_POST['non_payment'];
		
		$initial_amount = $_SESSION['MEM']['initial_amount'];
		$billing_amount = $_SESSION['MEM']['billing_amount'];
		$membership_id = $_SESSION['MEM']['membership_id'];
		$user_name = $_SESSION['MEM']['user_name'] ;
		$email_id = $_SESSION['MEM']['emailid'] ;
		$password = $_SESSION['MEM']['password'];
		$displayname = $_SESSION['MEM']['first_name'] . $_SESSION['MEM']['last_name'];
		$reg_date = date("Y-m-d h:i:s") ;
		
		$user_name = $_SESSION['MEM']['user_name'] ;
		$password = md5($_SESSION['MEM']['password']) ;
		$email_id = $_SESSION['MEM']['emailid'] ;
		
		$exp = $exp_number . " " .$exp_period ;
		$memstart = strtotime($reg_date) ;
		//$trialdate = strtotime('+7 days', $memstart);
		//$t_date = date('Y-m-d', strtotime($trialdate));
		
		$exp_raw = '+' . $exp;
		$exp_date = strtotime($exp_raw, $memstart);
		$expdate = date('Y-m-d H:i:s', $exp_date);
		
		$url = get_option('home').'/level';
		echo "<script>window.location='".$url."'</script>";
		
	}
?>
<script type="text/javascript" src="<?php echo plugins_url() ;?>/yantra_tv_subscription/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo plugins_url() ;?>/yantra_tv_subscription/js/validate.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	$("#signupForm").validate({
			rules: {
				firstname: "required",
				lastname: "required",
				username: {
					required: true,
					minlength: 2
				},
				password: {
					required: true,
					minlength: 5
				},
				confirm_password: {
					required: true,
					minlength: 5,
					equalTo: "#password"
				},
				email: {
					required: true,
					email: true
				},
				topic: {
					required: "#newsletter:checked",
					minlength: 2
				},
				agree: "required"
			},
			messages: {
				firstname: "Please enter your firstname",
				lastname: "Please enter your lastname",
				username: {
					required: "Please enter a username",
					minlength: "Your username must consist of at least 2 characters"
				},
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above"
				},
				email: "Please enter a valid email address",
				agree: "Please accept our policy"
			}
		});
		
		var x_timer;    
			jQuery("#username").change(function (){
			//alert('aaa');
				var user_name= jQuery(this).val();
				jQuery("#user-result").html('<img src="<?php echo plugins_url() ; ?>/yantra_tv_subscription/images/ajax-loader.gif" />');
				jQuery.post('<?php echo plugins_url() ; ?>/yantra_tv_subscription/username-checker.php', {'username':user_name}, function(data) {
			  	jQuery("#user-result").html(data);
				});
			}); 
		
		
		var x_email_timer;    
			jQuery("#emailid").change(function (){
			var emailid= jQuery(this).val();
				jQuery("#email-result").html('<img src="<?php echo plugins_url() ; ?>/yantra_tv_subscription/images/ajax-loader.gif" />');
				jQuery.post('<?php echo plugins_url() ; ?>/yantra_tv_subscription/email-checker.php', {'emailid':emailid}, function(data) {
			 	jQuery("#email-result").html(data);
			});
			}); 
		
});
</script>
<?php		
	$data='';
	$data.='<div class="yant_sub">';
	$data.='<div id="mem_level-'.$memlevel_id.'">' ;
	$data.='<form id="signupForm" class="mem_form" action="" method="post">' ;
	
	$data.='<div class="mem_label"><div class="adress">User name<span class="red">*</span> : </div>';
		$data.='<div class="field"><input type="text" id="username" name="user_name" placeholder="Username" class="required"/><span id="user-result"></span></div>';
	$data.='</div>';
	$data.='<div class="mem_label"><div class="adress">Email Id<span class="red">*</span> : </div>';
		$data.='<div class="field"><input type="text" name="emailid" id="emailid" placeholder="Email Id" class="email required"/><span id="email-result"></span></div>';
	$data.='</div>';
	$data.='<div class="mem_label"><div class="adress">First Name<span class="red">*</span> : </div>';
		$data.='<div class="field"><input type="text" name="first_name" placeholder="First Name" class="required"/></div>';
	$data.='</div>';
	$data.='<div class="mem_label"><div class="adress">Last Name<span class="red">*</span> : </div>';
		$data.='<div class="field"><input type="text" name="last_name" placeholder="Last Name" class="required"/></div>';
	$data.='</div>';
	$data.='<div class="mem_label"><div class="adress">Password<span class="red">*</span> : </div>';
		$data.='<div class="field"><input type="password" name="password" id="password" placeholder="Password" class="required"/></div>';
	$data.='</div>';
	$data.='<div class="mem_label"><div class="adress">Confirm Password<span class="red">*</span> : </div>';
		$data.='<div class="field"><input type="password" id="confirm_password" name="confirm_password" placeholder="Confirm Password" class="required"/></div>';
	$data.='</div>';
	$data.='<div class="clr"></div>
                <div class="mem_label">
                    <div class="adress">&nbsp;&nbsp;</div>
                    <div class="field" style="margin-top:10px;">
                        <div class="green-submit-btn">
                        	<input type="submit" name="registration" class="mem_signup_btn btnbox" value="Signup" /> 
                        </div>
                    </div>
                </div>';
	$data.='</form>' ;
	$data.='</div>' ;
	$data.='</div>' ;
	return $data;
}
add_shortcode("membership_signup", "signup_shortcode");