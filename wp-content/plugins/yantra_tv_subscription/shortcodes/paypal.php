<?php
//payment shortcode 

function paypal_shortcode($atts)
{	
global $post,$wpdb,$signature;
$level_detail = leveldetail($_SESSION['MEM']['mlevel']) ;

//print_r ($level_detail);
// sandbox test environment
$paypal_endpoint="https://www.sandbox.paypal.com/cgi-bin/webscr";
$paypal_merchant="vantag_1298611446_biz@gmail.com";

// live payment environment
/*
$paypal_endpoint="https://www.paypal.com/cgi-bin/webscr";
$paypal_merchant="real_merchant@real_domain.com";
*/

// webapp url configuration
$webapp_return="ydprojects.yantrait.co.uk/2015/yantra/ytv15/sucess";
$webapp_cancel="ydprojects.yantrait.co.uk/2015/yantra/ytv15/cancel";
$webapp_notify="ydprojects.yantrait.co.uk/2015/yantra/ytv15/notify";
	
$data='';
	$data.='<div class="yant_sub">';
	$data.='<form action="'.$paypal_endpoint.'" method="POST" id="paymentForm">
                <div id="container">
                    <center> <h3>Billing Information</h3></center>
                    <input type="hidden" value="'.$paypal_merchant.'" name="business">
		
					<input type="hidden" name="return" value="'.$webapp_return.'" />
					<input type="hidden" name="cancel_return" value="'.$webapp_cancel.'"  />
					<input type="hidden" name="notify_url" value="'.$webapp_notify.'"/>
					<input type="hidden" name="cmd" value="_xclick-subscriptions" />
					<input type="hidden" name="no_note" value="1" />
					<input type="hidden" name="no_shipping" value="1">
					<input type="hidden" name="currency_code" value="USD">
					<input type="hidden" name="country" value="IN" />
                    <table style="width:100%">
                         <tr>
                            <td id="td-label">Initial Amount( USD ) : </td>
                            <td><input type="text" name="a1" id="initial_amount" placeholder="Enter amount " value="'.$level_detail[0]->initial_payment.'" readonly="readonly"></td>		
						</tr>
						<tr>
                            <td id="td-label">Membership Name : </td>
                            <td>
								<input type="hidden" name="membership_id" id="membership_id" placeholder="Enter name " value="'.$level_detail[0]->id.'">
								<input type="text" name="item_name" id="membership" placeholder="Enter Name " value="'.$level_detail[0]->name.'" readonly="readonly">
							</td>		
						</tr>
						<input type="hidden" value="'.$paypal_merchant.'" name="business">
						
						<input type="hidden" name="p1" value="'.$level_detail[0]->trial_limit.'" />
						<input type="hidden" name="t1" value="D" />
						
						<tr>
							<td id="td-label">Recurring Amount : </td>
							<td><input type="text" name="a3" value="'.$level_detail[0]->billing_amount.'"  readonly="readonly" /><td>
						</tr>
						
						<tr>
							
							<td>	
								<input type="hidden" name="p3" value="'.$level_detail[0]->cycle_number.'" />
								<input type="hidden" name="t3" value="'.$level_detail[0]->cycle_period.'"/>
							</td>
						<tr>
						
						<tr>
							
							<td>
								<input type="hidden" name="src" value="1" />
								<input type="hidden" name="sra" value="1" />
							</td>
						</tr>
						
						<input type="hidden" name="custom" value="" />
                    </table>
					<center><!--<a href="#" id="paynow"> Pay Now </a>--><input  style="width: 20%;" type="submit"  name="submit" value="Pay Now" class="btnbox"></center><br>
                </div>
            </form>' ;
	$data.='</div>';
return $data;
	
}
add_shortcode("membership_paypal", "paypal_shortcode");