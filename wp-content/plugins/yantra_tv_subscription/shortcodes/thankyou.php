<?php
function thankyou_shortcode($atts)
{	
	global $post,$wpdb,$signature;
	$memlevel_id = $_SESSION['MEM']['mlevel'] ;
	$levelid = leveldetail($memlevel_id);
	
	$data='';
	$data.='<div class="yant_sub">';
	$data.='<div class="thankyou">';
	$data.='<h3 id="success">Thank You</h3>' ;
	$data.='<div class="subscribe_box">For Subcribing</div>' ;
	$data.='<div class="done_box">You are almost done</div>' ;
	$data.='<div class="confirm_box">Please check your email to confirm your subscription</div>' ;
	/*$data.='Amount -'.$levelid[0]->billing_amount;*/
	$data.='<div class="back_btn"><a href="'.get_option('home').'/videos'.'" class="btnbox">Click here to find out more</a></div>';
	$data.='</div>';
	$data.='</div>';
	return $data;
}
add_shortcode("membership_thankyou", "thankyou_shortcode");