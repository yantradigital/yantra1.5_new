<?php
//payment shortcode 

function payment_shortcode($atts)
{	
global $post,$wpdb,$signature;
$level_detail = leveldetail($_SESSION['MEM']['mlevel']) ;
?>
<script type="text/javascript" src="<?php echo plugins_url() ;?>/yantra_tv_subscription/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo plugins_url() ;?>/yantra_tv_subscription/js/validate.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	$("#paymentForm").validate({
			rules: {
				cardno: "required",
				cvv: {
					required: true,
					minlength: 3
				},
				address: "required",
				city: "required",
				state: "required",
				zipcode: {
					required: true,
					minlength: 3
				},
				country: "required",
				phone: {
					required: true,
					minlength: 10
				}
			},
			messages: {
				cardno: "Please enter cardno",
				cvv: {
					required: "Please enter cvv",
					minlength: "Your cvv must consist of at least 3 digit"
				},
				address: "Please enter address",
				city: "Please enter city",
				state: "Please enter state",
				zipcode: {
					required: "Please provide zipcode",
					minlength: "Your zipcode must be at least 3 characters long"
				},
				country: "Please enter country",
				phone: {
					required: "Please provide phone",
					minlength: "Your phoneno length must 10 digit"
				}
			}
		});
});
</script>
<?php
$mlevel = $_SESSION['MEM']['mlevel'];
$levelid = leveldetail($mlevel);
$sendemail=true;
if(isset($_POST['submit']))
{
	//print_r ($_POST) ;
	require_once('config/config.php');
	require_once('autoload.php');	
	require_once('angelleye/PayPal/PayPal.php');

$PayPalConfig = array(
					'Sandbox' => $sandbox,
					'APIUsername' => $api_username,
					'APIPassword' => $api_password,
					'APISignature' => $api_signature, 
                    'LogResults' => $log_results,
                    'LogPath' => $log_path,
					);
					
$PayPal = new angelleye\PayPal\PayPal($PayPalConfig);

$CRPPFields = array(
			'token' => '', 								// Token returned from PayPal SetExpressCheckout.  Can also use token returned from SetCustomerBillingAgreement.
				);

$DaysTimestamp = strtotime('now');
$Mo = date('m', $DaysTimestamp);
$Day = date('d', $DaysTimestamp);
$Year = date('Y', $DaysTimestamp);
$StartDateGMT = $Year . '-' . $Mo . '-' . $Day . 'T00:00:00\Z';

$ProfileDetails = array(
					'subscribername' => $_POST['firstName'], 				
					// Full name of the person receiving the product or service paid for by the recurring payment.  32 char max.
					'profilestartdate' => $StartDateGMT, 				
					// Required.  The date when the billing for this profiile begins.  Must be a valid date in UTC/GMT format.
					'profilereference' => '' 					
					// The merchant's own unique invoice number or reference ID.  127 char max.
				);	
				
$ScheduleDetails = array(
					'desc' => $_POST['membership'], 								// Required.  Description of the recurring payment.  This field must match the corresponding billing agreement description included in SetExpressCheckout.
					'maxfailedpayments' => '', 					// The number of scheduled payment periods that can fail before the profile is automatically suspended.  
					'autobillamt' => '0.00' 						// This field indiciates whether you would like PayPal to automatically bill the outstanding balance amount in the next billing cycle.  Values can be: NoAutoBill or AddToNextBilling
				);
				
$BillingPeriod = array(
					'trialbillingperiod' => $_POST['p1'], 
					'trialbillingfrequency' => '', 
					'trialtotalbillingcycles' => '', 
					'trialamt' => $_POST['initial_amount'], 
					'billingperiod' => 'Day', 						// Required.  Unit for billing during this subscription period.  One of the following: Day, Week, SemiMonth, Month, Year
					'billingfrequency' =>$_POST['t3'] , 					// Required.  Number of billing periods that make up one billing cycle.  The combination of billing freq. and billing period must be less than or equal to one year. 
					'totalbillingcycles' => $_POST['srt'], 				// the number of billing cycles for the payment period (regular or trial).  For trial period it must be greater than 0.  For regular payments 0 means indefinite...until canceled.  
					'amt' => $_POST['a3'], 								// Required.  Billing amount for each billing cycle during the payment period.  This does not include shipping and tax. 
					'currencycode' => 'USD', 						// Required.  Three-letter currency code.
					'shippingamt' => '', 						// Shipping amount for each billing cycle during the payment period.
					'taxamt' => '' 								// Tax amount for each billing cycle during the payment period.
				);
				
$ActivationDetails = array(
					'initamt' => $_POST['initial_amount'], 		// Initial non-recurring payment amount due immediatly upon profile creation.  Use an initial amount for enrolment or set-up fees.
					'failedinitamtaction' => '', 				// By default, PayPal will suspend the pending profile in the event that the initial payment fails.  You can override this.  Values are: ContinueOnFailure or CancelOnFailure
				);
				
$CCDetails = array(
					'creditcardtype' => $_POST['creditCardType'], 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
					'acct' => $_POST['creditCardNumber'], 								// Required.  Credit card number.  No spaces or punctuation.  
					'expdate' => $_POST['expDateMonth'].$_POST['expDateYear'], 							// Required.  Credit card expiration date.  Format is MMYYYY
					'cvv2' => $_POST['cvv2Number'], 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
					'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
					'issuenumber' => ''							// Issue number of Maestro or Solo card.  Two numeric digits max.
				);
				
$PayerInfo = array(
					'email' => $_POST['email'], 								// Email address of payer.
					'payerid' => '', 							// Unique PayPal customer ID for payer.
					'payerstatus' => '', 						// Status of payer.  Values are verified or unverified
					'countrycode' => '', 						// Payer's country of residence in the form of the two letter code.
					'business' => 'Testers, LLC' 							// Payer's business name.
				);
				
$PayerName = array(
					'salutation' => '', 						// Payer's salutation.  20 char max.
					'firstname' => $_POST['firstName'], 							// Payer's first name.  25 char max.
					'middlename' => '', 						// Payer's middle name.  25 char max.
					'lastname' => $_POST['lastName'], 							// Payer's last name.  25 char max.
					'suffix' => ''								// Payer's suffix.  12 char max.
				);
				
$BillingAddress = array(
						'street' => $_POST['address1'], 						// Required.  First street address.
						'street2' => $_POST['address2'], 						// Second street address.
						'city' => $_POST['city'], 							// Required.  Name of City.
						'state' => $_POST['state'], 							// Required. Name of State or Province.
						'countrycode' => $_POST['country'], 					// Required.  Country code.
						'zip' => $_POST['zip'], 							// Required.  Postal code of payer.
						'phonenum' => $_POST['phone'] 						// Phone Number of payer.  20 char max.
					);
					
$ShippingAddress = array(
						'shiptoname' => '', 					// Required if shipping is included.  Person's name associated with this address.  32 char max.
						'shiptostreet' => '', 					// Required if shipping is included.  First street address.  100 char max.
						'shiptostreet2' => '', 					// Second street address.  100 char max.
						'shiptocity' => '', 					// Required if shipping is included.  Name of city.  40 char max.
						'shiptostate' => '', 					// Required if shipping is included.  Name of state or province.  40 char max.
						'shiptozip' => '', 						// Required if shipping is included.  Postal code of shipping address.  20 char max.
						'shiptocountrycode' => '', 				// Required if shipping is included.  Country code of shipping address.  2 char max.
						'shiptophonenum' => ''					// Phone number for shipping address.  20 char max.
						);
						
$PayPalRequestData = array(
'ProfileDetails' => $ProfileDetails, 
'ScheduleDetails' => $ScheduleDetails, 
'BillingPeriod' => $BillingPeriod, 
'CCDetails' => $CCDetails, 
'PayerInfo' => $PayerInfo, 
'PayerName' => $PayerName, 
'BillingAddress' => $BillingAddress
);

$PayPalResult = $PayPal->CreateRecurringPaymentsProfile($PayPalRequestData);

		$username = $_SESSION['MEM']['user_name'] ;
		$emailid = $_SESSION['MEM']['emailid'] ;
		$first_name = $_SESSION['MEM']['first_name'];
		$last_name = $_SESSION['MEM']['last_name'] ;
		$password = md5($_SESSION['MEM']['password']);
		$initial_amount= $_SESSION['MEM']['initial_amount'] ;
		$billing_amount = $_SESSION['MEM']['billing_amount'] ;
		$membership_id = $_SESSION['MEM']['membership_id'] ;
		$initial_amount = $_SESSION['MEM']['initial_amount'];
		$billing_amount = $_SESSION['MEM']['billing_amount'];
		$membership_id = $_SESSION['MEM']['membership_id'];
		$user_name = $_SESSION['MEM']['user_name'] ;
		$email_id = $_SESSION['MEM']['emailid'] ;
		$password = $_SESSION['MEM']['password'];
		$displayname = $_SESSION['MEM']['first_name'] ." " . $_SESSION['MEM']['last_name'];
		$reg_date = date("Y-m-d h:i:s") ;
		
		$memlevel_id = $_SESSION['MEM']['mlevel'] ;
		$levelid = leveldetail($memlevel_id);
		$trial_limit = $levelid[0]->trial_limit ;
		$exp_number = $levelid[0]->expiration_number ;
		$exp_period = $levelid[0]->expiration_period ;
		$exp = $exp_number . " " .$exp_period ;
		$memstart = strtotime($reg_date) ;
		$trialdate = strtotime('+7 days', $memstart);
		$t_date = date('Y-m-d', strtotime($trialdate));
		//After trial period
		//$tdate = date('r', $t_date);
		//End date
		
		$exp_raw = '+' . $exp;
		$exp_date = strtotime($exp_raw, $trialdate);
		$expdate = date('Y-m-d H:i:s', $exp_date);
		
		$tablename=$wpdb->prefix.'memberships_users';
		$usertable=$wpdb->prefix.'users';
		$ordertable=$wpdb->prefix.'membership_orders';
		
		
		$billing_address1 = $_POST['address1'];
		$billing_address2 = $_POST['address2'];
		$billing_street = $billing_address1 . $billing_address1 ;
		$billing_city = $_POST['city'];
		$billing_state = $_POST['state'];
		$billing_countrycode = $_POST['country'];
		$billing_zip = $_POST['zip'];
		$billing_phone = $_POST['phone'];	
		
		$creditCardType = $_POST['creditCardType'] ;
		$creditCardNumber = $_POST['creditCardNumber'];
		$expyear = $_POST['expDateYear'];
		$expmonth = $_POST['expDateMonth'];
		
		$membershipid = $_POST['membership_id'];
		
		if($status == 'ActiveProfile')
		{
			$code = $PayPalResult['PROFILEID'] ;
			$ackresponse = $PayPalResult['ACK'];
			$status = $PayPalResult['PROFILESTATUS'] ;
			//Insert data in user table
			$sql = "INSERT INTO " . $usertable . " (`user_login`, `user_pass`, `user_nicename`, `user_email`,  `user_registered`,  `display_name`) VALUES ('$user_name', '$password', '$user_name', '$email_id',  '$reg_date',  '$displayname')" ;
			$result = $wpdb->query($sql);
			$last_id=$wpdb->insert_id;
			
			//Insert data in membership users table
			$sql1 = "INSERT INTO " . $tablename . " (`user_id`, `membership_id`, `initial_payment`, `billing_amount`, `trial_limit`, `status`, `startdate`, `enddate`, `modified`) VALUES ('$last_id', '$membership_id', '$initial_amount', '$billing_amount',  '$trial_limit',  'active', '$reg_date', '$expdate', '$reg_date')" ;
			$result1 = $wpdb->query($sql1);
			
			// Insert data in transaction table
			$sql2 = "INSERT INTO ". $ordertable ."(`code`, `user_id`, `membership_id`, `billing_name`, `billing_street`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `billing_phone`, `total`, `payment_type`, `cardtype`, `accountnumber`, `expirationmonth`, `expirationyear`, `status`, `timestamp`) VALUES ('$code', '$last_id', '$membershipid', '$displayname', '$billing_street', '$billing_city', '$billing_state', '$billing_zip', '$billing_countrycode', '$billing_phone', '$billing_amount', 'creditcard', '$creditCardType', '$creditCardNumber', '$expyear', '$expmonth', 'active', now())" ;
			if($sendemail)
			{
				//$sitename = get_option('blogname');
			    $sitename = 'Yantratv payment subscription' ;
				$to       = $_SESSION['MEM']['emailid'];	
				//print_r ($to); 
				//$to     = get_option('admin_email');	
				$subject  = $sitename ;	
				$from     = get_option('admin_email') ;
				$fromname = $sitename;
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\nFrom: $fromname <$from>\r\nReply-To: $from";
				
				$message="Dear ".wp_get_current_user()->display_name."<br /><br />";
				$message.="Your account has been activate.<br/><br/>";
				
				$message.="Your account e-mail:: ". $_SESSION['MEM']['emailid'] . "<br/>";
				$message.="Your account username: ". $_SESSION['MEM']['user_name'] . "<br/>";
				
				$message.="<a href='". get_option('home')."/login'>Confirmation Link</a>" ;
					
				$message.='<div style="clear:both;margin-top:20px;"></div>Best Regards!<br/>';
				//echo 'test' ;	
				$sent_message = wp_mail($to, $subject, $message, $headers);
			}
			$url=get_option('home').'/thankyou';
			echo"<script>window.location='".$url."'</script>";	
		}
		else
		{
			//$sitename = get_option('blogname');
			    $sitename = 'Yantratv payment subscription' ;
				$to       = $_SESSION['MEM']['emailid'];	
				//print_r ($to); 
				//$to     = get_option('admin_email');	
				$subject  = $sitename ;	
				$from     = get_option('admin_email') ;
				$fromname = $sitename;
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\nFrom: $fromname <$from>\r\nReply-To: $from";
				
				$message="Dear ".wp_get_current_user()->display_name."<br /><br />";
				$message.="Your payment detail is failed.<br/><br/>";
				
				$message.="<a href='". get_option('home')."/login'>Confirmation Link</a>" ;
					
				$message.='<div style="clear:both;margin-top:20px;"></div>Best Regards!<br/>';
				//echo 'test' ;	
				$sent_message = wp_mail($to, $subject, $message, $headers);
				echo "<h3 id='fail'>Payment - Failed</h3>";
				echo '<p>Error msg - This transaction cannot be processed. <br>Please enter a valid credit card number and type.</p>';
		}

//echo '<pre />';
//print_r($PayPalResult);	
//die();
}		
				
												
						
	$data='';
	$data.='<div class="yant_sub">';
		$data.='<form action="" method="POST" id="paymentForm">
                <div id="container">
                    <h2>Pay with my debit or credit card</h2>
                    <hr/>
                    <center> <h3>Billing Information</h3></center>
                    <input type="hidden" name="paymentType" value="Sale"/>
                    <input type="hidden" name="id" value=""/>
                    <table style="width:100%">
                        <tr>
                            <td id="td-label">First name : </td>
                            <td><input type="text" name="firstName" id="name" placeholder="Enter first name" value="'.$_SESSION['MEM']['first_name'].'" class="required"></td>		
                        </tr>
                        <tr>
                            <td id="td-label">Last name : </td>
                            <td><input type="text" name="lastName" id="name" placeholder="Enter last name" value="'.$_SESSION['MEM']['last_name'].'" class="required"></td>		
                        </tr>
						<tr>
                            <td id="td-label">Email Id : </td>
                            <td><input type="text" name="email" id="emailid" placeholder="Enter emailid" value="'.$_SESSION['MEM']['emailid'].'" class="required"></td>		
                        </tr>
                        <tr>
                            <td id="td-label">Card type : </td>
                            <td>
                                <select name="creditCardType" class="required">
                                    <option value="Visa" selected="selected">Visa</option>
                                    <option value="MasterCard">MasterCard</option>
                                    <option value="Discover">Discover</option>
                                    <option value="Amex">American Express</option>
                                </select>
                            </td>		
                        </tr>
                        <tr>
                           <td id="td-label">Card number : </td>
                            <td><input type="text" name="creditCardNumber" id="cardno" placeholder="Enter card number" class="required"></td>		
                        </tr>
                        <tr>
                           <td id="td-label">Expiry date : </td>
                            <td><div id="date-div"><select name="expDateMonth">
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select></div>
                                <div id="date-div">
                                    <select name="expDateYear">					
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>						
                                    </select>
                                </div></td>		
                        </tr>
                        <tr>
                            <td id="td-label">CVV : </td>
                            <td><input type="text" name="cvv2Number" id="cvv" placeholder="cvv" class="required"></td>		
                        </tr>
                        <tr>
                            <td id="td-label">Initial Amount( USD ) : </td>
						
						<td>
						<input type="hidden" name="p1" value="'.$level_detail[0]->trial_limit.'" />
						<input type="hidden" name="t1" value="D" />
						<input type="text" name="initial_amount" id="initial_amount" placeholder="Enter amount " value="'.$level_detail[0]->initial_payment.'" readonly="readonly"></td>		
						</tr>
						<tr>
							<td id="td-label">Recurring Amount : </td>
							<td><input type="text" name="a3" value="'.$level_detail[0]->billing_amount.'"  readonly="readonly"/><td>
						</tr>
								<input type="hidden" name="p3" value="1" />
								<input type="hidden" name="src" value="1" />
								<input type="hidden" name="srt" value="'.$level_detail[0]->cycle_number.'" />
								<input type="hidden" name="sra" value="1" />
						<tr>
                            <td id="td-label">Membership Name : </td>
                            <td>
							<input type="hidden" name="membership_id" id="membership_id" placeholder="Enter name " value="'.$_SESSION['MEM']['mlevel'].'">
							<input type="text" name="membership" id="membership" placeholder="Enter Name " value="'.$level_detail[0]->name.'" readonly="readonly"></td>		
						</tr>
                    </table>

                    <center> <h3>Billing address</h3></center>

                    <table style="width:100%">
                        <tr>
                            <td id="td-label">Address 1 : </td>
                            <td><input type="text" name="address1" id="address" placeholder="Enter address" class="required"></td>		

                        </tr>
                        <tr>
                            <td id="td-label">Address 2 : </td>
                            <td><input type="text" name="address2" id="address2" placeholder="Enter address"></td>		

                        </tr>
                        <tr>
                            <td id="td-label">City : </td>
                            <td><input type="text" name="city" id="city" placeholder="Enter city name"></td>	

                        </tr>
                        <tr>
                            <td id="td-label">State : </td>
                            <td>
                                <input type="text" name="state" id="state" placeholder="Enter state" class="required"/> 
                            </td>		
                        </tr>
                        <tr>
                            <td id="td-label">Zip code : </td>
                            <td>
                                <input type="text" name="zip" id="zipcode" placeholder="Enter zip code  (5 or 9 digits)" class="required">
                            </td>		

                        </tr>
                        <tr>
                            <td id="td-label">Country : </td>
                            <td><input type="text" name="country" id="country" placeholder="Enter country name" class="required"></td>		
                        </tr>
                        <tr>
                            <td id="td-label">Phone Number : </td>
                            <td><input type="text" name="phone" id="phone" placeholder="Enter Phone number" class="required number"></td>		
                        </tr>
                    </table><br>
                    <center><!--<a href="#" id="paynow"> Pay Now </a>--><input  style="width: 20%;" type="submit"  name="submit" value="Pay Now" class="btnbox"></center><br>
                </div>
            </form>' ;
	$data.='</div>';
	return $data;
	
}
add_shortcode("membership_payment", "payment_shortcode");