<?php if(!isset($_REQUEST['mem']) )
{ 
	global $wpdb,$signature;
	$prefix=$wpdb->base_prefix;
	$blog_id = $wpdb->blogid;
	
	$totalrec=20;
	if(isset($_REQUEST['pagedid']) && $_REQUEST['pagedid']>1)
	{
		$pageid=$_REQUEST['pagedid'];
		$limitstart=$totalrec*($pageid-1);
	}
	else
	{
		$pageid=1;
		$limitstart=0;
		$limitsend=$totalrec;
	}
	
	$where=" order by id desc";
	$querystr = "SELECT * FROM ".$prefix."memberships_users $where limit $limitstart, $totalrec";
	$users = $wpdb->get_results($querystr, OBJECT);
	
	$querystr = "SELECT * FROM ".$prefix."memberships_users $where";
	$totalusers = $wpdb->get_results($querystr, OBJECT);
?>

<link rel="stylesheet" href= "<?php echo plugins_url() ; ?>/yantra_tv_subscription/css/style.css" type='text/css' media='all' />

<?php $url=get_option('home').'/wp-admin/admin.php?page=MembersList'; ?>
<div class="wrap">
<?php echo "<h2>" . __( 'Members List', 'webserve_trdom' ) . "</h2>"; ?>
<a href="<?php _e($url); ?>&mem=cancelmembers"><div class="add-new btnbox cncelbox" style="margin: 0.83em 0.5em;">Cancelled Members</div></a>

<div class="clr"></div>

<form name="conatct_form" method="post" onSubmit="return check_blank();" action="<?php echo $url; ?>">
<input type="hidden" name="usr" value="filter" />
<div style="clear:both; height:20px;" class="yant_sub"></div>
	<table class="membership-levels" width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #ccc;">
		<tr>
			<th valign="top" align="left" width="60">&nbsp;<?php _e("ID" ); ?></th>
			<th valign="top" align="left"><?php echo("User Name" ); ?></th>
            <th valign="top" align="left"><?php echo("First Name" ); ?></th>
			<th valign="top" align="left"><?php echo("Last Name" ); ?></th>
            <th valign="top" align="left"><?php echo("Email" ); ?></th>
            <th valign="top" align="left"><?php echo("Billing Address" ); ?></th>
            <th valign="top" align="left"><?php echo("Membership" ); ?></th>
            <th valign="top" align="left"><?php echo("Fee" ); ?></th>
            <th valign="top" align="left"><?php echo("Joined" ); ?></th>
            <th valign="top" align="left"><?php echo("Expires" ); ?></th>
            <th valign="top" align="left"><?php echo("Status" ); ?></th>
		</tr>
	<?php 
	$cnt=$limitstart+1; 
	foreach($users as $user)
	{ 
		$userdata = get_userdata($user->user_id);
		$join_date = $userdata->user_registered;
		$memid = leveldetail($user->membership_id) ;
		$orderid = orderdetail($user->user_id);
	?>
	  <?php echo '<tr class="' . ($cnt%2 ? 'odd':'even') . '">' ; ?>
		<td valign="top" align="left">&nbsp;<?php _e($cnt); ?></td>
        <td valign="top" align="left"><?php echo($userdata->user_nicename); ?></td>
		<td valign="top" align="left"><?php echo($userdata->first_name); ?></td>
        <td valign="top" align="left"><?php echo($userdata->last_name); ?></td>
        <td valign="top" align="left"><?php echo($userdata->user_email); ?></td>
        <td valign="top" align="left"><?php echo($orderid[0]->billing_street.$orderid[0]->billing_city.$orderid[0]->billing_state); ?></td>
        <td valign="top" align="left"><?php echo($memid[0]->name); ?></td>
        <td valign="top" align="left"><?php echo($user->billing_amount); ?></td>
        <td valign="top" align="left"><?php echo(date('Y-m-d', strtotime($join_date))); ?></td>
        <td valign="top" align="left"><?php echo($user->enddate); ?></td>
        <td valign="top" align="left"><?php echo($user->status); ?></td>
	  </tr>
	  <?php $cnt++; } ?>
	</table>
</form>
<?php if(count($totallevels)>$totalrec){ ?>
<div style="float:left; margin-top:10px;" class="pagination">

<?php if($pageid>1){ ?><a href="<?php _e($url); ?>" title="First" class="fl"><img src="<?php echo get_option('home');?>/wp-content/plugins/yantra_tv_subscription/images/first.png" alt="First" title="First" /></a><?php } ?>
    <?php $totalpages=ceil(count($totallevels)/$totalrec);
			
			$previous = $pageid-1;
			if($previous>0)
			{
				?>
				<a class="fl" href="<?php _e($url.'&amp;pagedid='.$previous);?>"><img src="<?php echo get_option('home');?>/wp-content/plugins/yantra_tv_subscription/images/previous.png" alt="previous" title="previous" /></a>
				<?php
			}
			?>
            <div class="fl ml5 mr10">Page Number:</div>
            <div class="fl mr5">
            	<script type="text/javascript">
				//<![CDATA[
					jQuery(document).ready( function(){
						jQuery('#paginate').live('change', function(){
							var pagedid=jQuery(this).val();
							window.location='<?php _e($url); ?>&pagedid='+pagedid;
						});
					})
					//]]>
				</script>
                <select style="float:left;" id="paginate" name="pagedid">
                <?php for($k=1;$k<=$totalpages;$k++){ ?>
                    <option value="<?php _e($k); ?>" <?php if($k==$pageid){ _e('selected="selected"');}?>><?php _e($k); ?></option>
                <?php } ?>
                </select>
           	</div>
			<?php
				
			
			$next = $pageid+1;
			if($totalpages>=$next)
			{
				?>
				<a class="fl" href="<?php _e($url.'&amp;pagedid='.$next);?>"><img src="<?php echo get_option('home');?>/wp-content/plugins/yantra_tv_subscription/images/next.png" alt="next" title="next" /></a>
				<?php
			}
     ?>
     <?php if($totalpages>$pageid){ ?><a href="<?php _e($url.'&amp;pagedid='.$totalpages); ?>" title="Last" class="fl"><img src="<?php echo get_option('home');?>/wp-content/plugins/yantra_tv_subscription/images/last.png" alt="Last" title="Last" /></a><?php } ?>

</div>
<?php } ?>
</div>

<?php } ?>