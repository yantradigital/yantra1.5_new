<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ytv_new');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'MQ)YVbB}zgY_|YULe0ykVb,sYt`>0P5^I;go 3>O >HbR`sz 3g87Cl_Fcb93|Az');
define('SECURE_AUTH_KEY',  'NM!QeE`c:WL:OalweX2IQHY|GrkL9-A@J`/-GH~KN1 csD7m&xx(8N,6VB=%#w:1');
define('LOGGED_IN_KEY',    'z;Wn+$ofdGq5IVVH0S6eyStwv_!n3d#m&x.gdd|C[h^rzL&XX19ikW-KPU&H}mg*');
define('NONCE_KEY',        '<xESCZ5%4$|?O=3KmK,CUqb?ryI(?AFcJ=VX#RPIjmfwk3,_P|.{!5L~U)l^x^G>');
define('AUTH_SALT',        'a$@RW;_`p.C7zkd.CgW?[iZ?wXksZDdU/ecvkKca.b+7=peoaV3}Rc ZM6;bXEq^');
define('SECURE_AUTH_SALT', '8!{4nsRI}@onV4u@IhlmenZZE-bz21^iDw#{|x7wQr)m>oBF@+Kyza^Z&?+*G D3');
define('LOGGED_IN_SALT',   '-W`t!~v(QDKR>MU-,RSuPh&%Fa_/<W,eh32v:bowhgc?]VU#oIY@HMqH6AZ. 0+F');
define('NONCE_SALT',       'i J-JRc.0>S:Q0}lcU5+kF)TxV.<~=[^wn$EzcvWTefsb yS$Vk*1?y9]RJx3uk{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
