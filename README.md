== Vzaar media management and Subscription Plugin ==

Vzaar Media Management is a full integrated media plugin for WordPress. Using this plugin one can Add/Edit/Delete media (video & audio) to vzaar and embed the media (video & audio) to any post or page.

Subscription plugin: It is an custom plugin for if user is not logged then after few seconds show popup for signup or login. 

== Features Vzaar videos ==

1. Vzaar API configuration
2. Add/Edit/Delete Media (video & audio) to vzaar
3. Embed media (video & audio) to your post. 
4. You can use this plugin with media library.
5. Add video in any post.
6. User can see own videos download by him/her
7. Videos filter is there. Admin can search videos by category/user/name
8.

== Features Subscription Plugin ==
1. Subscription plugin for assign any video to any level
2. For create level you can choose free or paid level
3. Create/Edit level you can choose trial period for subscription level
4. If you want trial period paid of free. for free choose initial amount 0.00 else amount
5. In this plugin there is feature for recurring amount

== Installation Vzaar media management plugin ==

1. Upload the files to wp-content/plugins/vzaar-media-management
2. Activate the plugin
3. Click on "VZAAR MEDIA" or "Settings" and set your VZAAR API Key
3. Click on "Add Media" and upload some media
4. Go to your post/page an click "Add Video" Flash media icon (at the top of the editor). Then there you will get the Tab "Vzaar Media".

== Installation Yantra tv subscription plugin ==

1. Upload the files to wp-content/plugins/yantra_tv_subscription
2. Activate the plugin